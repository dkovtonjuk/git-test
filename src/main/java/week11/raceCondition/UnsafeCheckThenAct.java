package week11.raceCondition;

public class UnsafeCheckThenAct {

    private int number = 0;

    public synchronized void changeNumber() {
        if (number == 0) {
            System.out.println(Thread.currentThread().getName() + " | is zero");
            number = -1;
        } else {
            System.out.println(Thread.currentThread().getName() + " | is -1");
        }


    }

    public static void main(String[] args) {
        final UnsafeCheckThenAct chrckAct = new UnsafeCheckThenAct();

        for (int i = 0; i < 50; i++) {

            Runnable runnable = new
                    Runnable() {
                        @Override
                        public void run() {
                            chrckAct.changeNumber();
                        }
                    };

            Thread thread = new Thread(runnable, "T" + i);
            thread.start();
;        }
    }
}
