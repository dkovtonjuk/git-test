package week11.raceCondition;

import java.util.concurrent.atomic.AtomicInteger;

public class UsanfeReadModifyWrite {
    private AtomicInteger number =  new AtomicInteger(0);

    public void incrementNumber() {
        number.incrementAndGet();
    }

    public int getNumber() {return this.number.get(); }

    public static void main(String[] args) throws  InterruptedException {

        final UsanfeReadModifyWrite rnw = new UsanfeReadModifyWrite();
        for (int i = 0; i < 1000; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() { rnw.incrementNumber();}

                }, "T" + i).start();
            }
Thread.sleep(2000);
        System.out.println("final number (should be 1000) :" + rnw.getNumber());
        }
    }


