package week11.multiThreading;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WriterToFileThread extends Thread{
    private  String  filename;
    private String text;

    public WriterToFileThread(String filename, String text) {
        this.filename = filename;
        this.text = text;
    }

    @Override
    public void run() {
       try( Writer writer = new FileWriter(new
                File(filename))) {
            System.out.println("working in thread" + currentThread().getName() );
           Thread.sleep(5000L);
            writer.write(text);

        } catch (IOException e) {
           e.printStackTrace();
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
    }
}
