package week11.multiThreading;



import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Main {
    public static void main(String[] args) {
        String text = "knmkj njnj njnj njnkjn kljnj nknkin. erfwerf qwerf qrf. q3rf q3rf q3f. q3f q3fr q34 3rf. oiuhohb ouhikuhikuhiuh iiuhiuhiuhiuh iouhiuhiuhiouhiuh oiuhouihiuygyutfiuhhoijiuguyfiohoijuiugutdfihoihiy iugiygoihiug iuhiug. grtgrtgrtgrtgrtg. rtrtgwefdweferggfergf.";
        String[] array = text.split("\\.");




        String path = "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/multiThreading/text-file";
        WriterToFileThread writer1 = new WriterToFileThread(path + 1, array[0]);
        WriterToFileThread writer2 = new WriterToFileThread(path + 2, array[1]);
        WriterToFileThread writer3 = new WriterToFileThread(path + 3, array[2]);
        WriterToFileThread writer4 = new WriterToFileThread(path + 4, array[3]);
        WriterToFileThread writer5 = new WriterToFileThread(path + 5, array[4]);
        WriterToFileThread writer6 = new WriterToFileThread(path + 6, array[5]);
        WriterToFileThread writer7 = new WriterToFileThread(path + 7, array[6]);

long before = System.currentTimeMillis();

        writer1.start();
        writer2.start();
        writer3.start();
        writer4.start();
        writer5.start();
        writer6.start();
        writer7.start();

        try {
            writer1.join();
            writer2.join();
            writer3.join();
            writer4.join();
            writer5.join();
            writer6.join();
            writer7.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("finished");
        System.out.println(System.currentTimeMillis() - before);
 }

    long after = System.currentTimeMillis();

    public static void writeEachSentanceToFile(String[] arr){
        String fileName = "file-";
        int fileCounter = 1;
        String path = "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/multiThreading";

        for (int i = 0; i < arr.length; i++) {
            File file = new File(path  +"/"+ fileName +  fileCounter + "/");
            try(Writer writer = new FileWriter(file)) {

                writer.write(arr[i]);
            } catch (IOException  e) {
                e.printStackTrace();
            }
            fileCounter++;
        }

    }


}


