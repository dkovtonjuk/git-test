package week11.multiThreading;

public class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println("I am working in " + Thread.currentThread().getName());
    }
}
