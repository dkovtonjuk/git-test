package week11.urlHolder;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        UrlsHolder urlsHolder = new UrlsHolder("https://pitchfork.com/reviews/albums/oneohtrix-point-never-love-in-the-time-of-lexapro-ep/", "http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html", "https://habr.com/company/neoflex/blog/431104/");

        UrlDownloader urlDownloader1 = new UrlDownloader(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder/urls_");
        UrlDownloader urlDownloader2 = new UrlDownloader(urlsHolder, "//home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder/urls_");
        UrlDownloader urlDownloader3 = new UrlDownloader(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder/urls_");
        UrlDownloader urlDownloader4 = new UrlDownloader(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder/urls_");


        urlDownloader1.start();
        urlDownloader2.start();
        urlDownloader3.start();
        urlDownloader4.start();

        urlDownloader1.join();
        urlDownloader2.join();
        urlDownloader3.join();
        urlDownloader4.join();

        System.out.println("Finished");
    }
}
