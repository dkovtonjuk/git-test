package week11.url_downloader;

import week11.urlHolder_new.UrlsHolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;

public class UrlDownloaderCallable implements Callable<String> {

    private UrlsHolder1 urlsHolder;
    private String path;

    public UrlDownloaderCallable(UrlsHolder1 holder, String path) {
        this.urlsHolder = holder;
        this.path = path;
    }



    @Override
    public String call() throws Exception {

        try {
            String stringUrl = urlsHolder.getNextUrl();

            if (stringUrl != null) {
                URL url = new URL(stringUrl);
                String contentOfUrl = readUrlToString(url);
               return saveContentToFile(contentOfUrl, this.path);

            } else {
                System.out.println("Queue is empty!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String readUrlToString(URL url) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String line;
            StringBuilder sb = new StringBuilder();

            System.out.println(String.format("Downloading url %s in %s", url.toString(), Thread.currentThread().getName()));

            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
            }
            return sb.toString();
        } catch (IOException e) {
            throw e;
        }
    }

    private String saveContentToFile(String content, String path) throws IOException, InterruptedException {
        Thread.sleep(ThreadLocalRandom.current().nextInt(5000));
        Path file = Paths.get(path + "//" + urlsHolder.incrementUrlsCount() + ".html");
        System.out.println(String.format("Saving content to %s in %s", file.toString(), Thread.currentThread().getName()));
        Files.write(file, content.getBytes());
       return file.toAbsolutePath().toString();
    }


}
