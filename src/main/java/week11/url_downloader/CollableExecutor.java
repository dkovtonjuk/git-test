package week11.url_downloader;

import java.util.concurrent.*;

public class CollableExecutor {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Callable<Integer> task = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println(Thread.currentThread().getName());
                int randomMiss = ThreadLocalRandom.current().nextInt(5000);
                Thread.sleep(randomMiss);
                return ThreadLocalRandom.current().nextInt(1000);
            }
        };

Callable<Integer> task2 = new Callable<Integer>() {
    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName());
        int randomMiss = ThreadLocalRandom.current().nextInt(5000);
        Thread.sleep(randomMiss);
        return ThreadLocalRandom.current().nextInt(1000);
    }
};


        Future<Integer> future = executorService.submit(task);
        Future<Integer> future2 = executorService.submit(task2);
       Integer result =  future.get();
        Integer result2 =  future2.get();
        System.out.println(result);
        System.out.println(result2);
    }
}
