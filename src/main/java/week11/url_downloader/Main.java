package week11.url_downloader;

import week11.urlHolder_new.UrlsHolder;

import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
       UrlsHolder1 urlsHolder1 = new UrlsHolder1("https://pitchfork.com/reviews/albums/oneohtrix-point-never-love-in-the-time-of-lexapro-ep/", "http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html", "https://habr.com/company/neoflex/blog/431104/");

        Callable<String> downloaderTask = new UrlDownloaderCallable(urlsHolder1, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/url_downloader/urls_");
        Callable<String> downloaderTask2 = new UrlDownloaderCallable(urlsHolder1, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/url_downloader/urls_");


        Future <String> future = executorService.submit(downloaderTask);
        Future <String> future2 = executorService.submit(downloaderTask2);


        System.out.println();
        try {
            System.out.println(String.format("I downloaded to %s", future.get()));
            System.out.println(String.format("I downloaded to %s", future2.get()));
            executorService.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


    }

    }

