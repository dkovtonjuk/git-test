package week11.url_downloader;

import java.util.PriorityQueue;
import java.util.Queue;

class UrlsHolder1 {
    private Queue<String> urlsQueue = new PriorityQueue<>();
    private int counter = 0;

    public UrlsHolder1(String... urls) {
        for (String url : urls) {
            urlsQueue.add(url);
        }
    }

    public synchronized String getNextUrl() {
        return urlsQueue.poll();
    }

    public synchronized int incrementUrlsCount() {
        return counter++;
    }

}