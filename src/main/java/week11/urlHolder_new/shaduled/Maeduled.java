//package week11.urlHolder_new;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.TimeUnit;
//
//public class ManyWithScheduled {
//    public static void main(String[] args) {
//        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//
//        Runnable task = () -> {
//            System.out.println("Files in folder: " + new File("/home/kevinteacher/IdeaProjects/elementary_vlad/elementary_/elementary/src/main/java/week_11/multithreading/executor/url_downloader/urls").list().length);
//        };
//
//        executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS); //start to do this task from now (0) with interval 1 second
//
//
//        ExecutorService executorService = Executors.newFixedThreadPool(5);
//
//
//        UrlsHolder urlsHolder = new UrlsHolder("https://pitchfork.com/reviews/albums/oneohtrix-point-never-love-in-the-time-of-lexapro-ep/", "http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html", "https://habr.com/company/neoflex/blog/431104/");
//        List<UrlDownloaderCallable1> callables = new ArrayList<>();
//
//        callables.add(new UrlDownloaderCallable1(urlsHolder, "/home/kevinteacher/IdeaProjects/elementary_vlad/elementary_/elementary/src/main/java/week_11/multithreading/executor/url_downloader/urls"));
//        callables.add(new UrlDownloaderCallable1(urlsHolder, "/home/kevinteacher/IdeaProjects/elementary_vlad/elementary_/elementary/src/main/java/week_11/multithreading/executor/url_downloader/urls"));
//        callables.add(new UrlDownloaderCallable1(urlsHolder, "/home/kevinteacher/IdeaProjects/elementary_vlad/elementary_/elementary/src/main/java/week_11/multithreading/executor/url_downloader/urls"));
//        callables.add(new UrlDownloaderCallable1(urlsHolder, "/home/kevinteacher/IdeaProjects/elementary_vlad/elementary_/elementary/src/main/java/week_11/multithreading/executor/url_downloader/urls"));
//
//
//        executorService.invokeAll(callables<UrlDownloaderCollable>);
//
//           // executorService.invokeAll(callables);
//
//
//
//        executorService.shutdown();
//        executor.shutdown();
//
//    }
//}