package week11.urlHolder_new.shaduled;




import week11.urlHolder_new.UrlsHolder;
import week11.url_downloader.UrlDownloaderCallable;

import java.io.File;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws Exception {

   ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        System.out.println("Files in folder: " + new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url").list().length);

        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("sheduling: " + System.nanoTime());
            }
        };

        executor.scheduleWithFixedDelay(task,0,2, TimeUnit.SECONDS); //do this task from now (0) with timeout of 2 seconds

        for (int i = 0; i < 100; i++) {
            Thread.sleep(100);
            System.out.println(i + " in " + Thread.currentThread().getName());
        }
        
        
//        UrlsHolder urlsHolder = new UrlsHolder("https://pitchfork.com/reviews/albums/oneohtrix-point-never-love-in-the-time-of-lexapro-ep/", "http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html", "https://habr.com/company/neoflex/blog/431104/");
//
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//
//
//
//        Callable<String> downloaderTask = new UrlDownloaderCallable(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/urls_");
//        UrlDownloaderCallable urlDownloader2 = new UrlDownloaderCallable(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/urls_");
//        UrlDownloaderCallable urlDownloader3 = new UrlDownloaderCallable(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/urls_");
//        UrlDownloaderCallable urlDownloader4 = new UrlDownloaderCallable(urlsHolder, "/home/kevinpc3/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/urls_");
//
//        Future<String> future = executorService.submit(downloaderTask );
//
//        String result =  future.get();
//        System.out.println(result);



    }
}
