package week11.urlHolder_new;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class UrlsHolder  {
    private Queue<String> urlsQueue = new PriorityQueue<>();
    private int counter = 0;

    public UrlsHolder(String... urls) {
        for (String url : urls) {
            urlsQueue.add(url);
        }
    }

    public synchronized String getNextUrl() {
        return urlsQueue.poll();
    }

    public synchronized int incrementUrlsCount() {
        return counter++;
    }

}