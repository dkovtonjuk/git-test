package week11.urlHolder_new;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Many {

    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);  //          executor 1

Runnable task = new Runnable() {
    @Override
    public void run() {
        System.out.println("files in folder: " + new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url").list().length);
            }
};
executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);


        ExecutorService executorService = Executors.newFixedThreadPool(5); //5 threads            executor 2

        UrlsHolder urlsHolder = new UrlsHolder("https://pitchfork.com/reviews/albums/oneohtrix-point-never-love-in-the-time-of-lexapro-ep/", "http://tutorials.jenkov.com/java-concurrency/race-conditions-and-critical-sections.html", "https://habr.com/company/neoflex/blog/431104/", "https://habr.com/company/neoflex/blog/431104/", "https://habr.com/company/neoflex/blog/431104/");

        List<UrlDownloaderCallable> callables = new ArrayList<>();

        callables.add(new UrlDownloaderCallable(urlsHolder, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url"));
        callables.add(new UrlDownloaderCallable(urlsHolder, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url"));
        callables.add(new UrlDownloaderCallable(urlsHolder, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url"));
        callables.add(new UrlDownloaderCallable(urlsHolder, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url"));
        callables.add(new UrlDownloaderCallable(urlsHolder, "/home/dmytro/IdeaProjects/gittest/src/main/java/week11/urlHolder_new/url"));


        try {
            List<Future<String>> futures = executorService.invokeAll(callables);
            for (Future<String> future : futures) {
             System.out.println(future.get());

                executorService.shutdown();

            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();

        }

executorService.shutdown();
        executor.shutdown();
    }
}



