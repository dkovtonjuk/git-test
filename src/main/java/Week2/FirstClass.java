package Week2;

import java.util.Arrays;

public class FirstClass {
    public static void main(String[] args) {
        System.out.println("Dmytro");

    }


    static void multiDimentialTriangle(){
String[][] multi = new String[6][];
int sharpSize = 0;

        for (int i = 0; i < multi.length; i++) {
         String[] inside = new String[++sharpSize];

            for (int j = 0; j < inside.length; j++) {
              inside[j] = "#";
            }
            multi[i] = inside;
        }
        for (int i = 0; i < multi.length; i++) {
            System.out.println(Arrays.toString(multi[i]));
        }
    }

    public static int[] alphabetNumbers(String string){
       int[] resultArray = new int[string.length()];
       char[] chars = string.toUpperCase().toCharArray();

        for (int i = 0; i < chars.length; i++) {
            int number = ((int) chars[i] - 64); //getting int from char

            if (number > 26 || number < 1) {
                number = -1;
            }
            resultArray[i] = number;
        }

        return resultArray;

    }
}
