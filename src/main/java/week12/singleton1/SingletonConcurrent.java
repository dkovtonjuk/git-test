package week12.singleton1;

public class SingletonConcurrent {
    private static volatile SingletonConcurrent instanse = null;

    private SingletonConcurrent() {

    }

    public static SingletonConcurrent getInstance() {
        if (instanse == null) {
            synchronized (SingletonConcurrent.class) {
                if (instanse == null) {
                    instanse = new SingletonConcurrent();
                }
            }
        }
        return instanse;
    }
}
