package week12;

public class IntegerToStringMain {
    public static void main(String[] args) {
System.out.println(test(4, y -> "now string " + String.valueOf(y)));
        System.out.println(test(4, y -> "" + y));
    }

    static String test (int i, IntegerToStringInterface integerToStringInterface){
        return integerToStringInterface.intToString(i);
    }
}
