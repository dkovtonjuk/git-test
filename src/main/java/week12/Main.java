package week12;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Arrays.asList("a", "b", "c").forEach(e -> System.out.println(e));

        Arrays.asList("a", "b", "c").forEach((String e) -> System.out.println(e));

        Arrays.asList("a", "b", "c").forEach((String e) -> {
            System.out.println(e);
            System.out.println(e);
            System.out.println(e);
            System.out.println(e);
        });

    }
}
