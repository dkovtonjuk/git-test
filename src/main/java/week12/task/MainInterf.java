package week12.task;

public class MainInterf {

    public static void main(String[] args) {



int[] array = {1, 2, 3, 4};
for (int i : array) {
    test(i, (j) -> j*2);
}

    }

    static void test (int i, IntMapperInterface intMapperInterface) {

        System.out.println(intMapperInterface.map(i));
    }
}
