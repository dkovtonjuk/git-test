package week12.task_2;

@FunctionalInterface
public interface Predicate<T> {

  boolean test(T t);

}
