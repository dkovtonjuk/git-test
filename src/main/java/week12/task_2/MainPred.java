package week12.task_2;

class Tester<T> {
    public static void main(String[] args) {

        Tester<Integer> tester = new Tester<>();
        System.out.println( tester.filter(4, i->  i % 2 ==0));

        Tester<String> tester1 = new Tester<>();
        System.out.println(  tester1.filter("blah", s -> s.isEmpty()));
    }

        boolean filter (T t, Predicate < T > predicate){
            return predicate.test(t);

    }
}
