package week12.lesson061218;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List <Person> persons = Arrays.asList(
                new Person("Max", 18),
                new Person("David", 23),
                new Person("Pamela", 23),
                new Person("David", 12));

        Map<Integer, List<Person>> personsByAge = persons
                .stream()
                .collect(Collectors.groupingBy(p -> p.getAge()));
        System.out.println(personsByAge.toString());
      //  String phrase

        Double averageAge = persons
                .stream()
                .collect(Collectors.averagingInt(p-> p.getAge()));
        System.out.println(averageAge);


       Integer ageSum = persons
               .stream()
               .reduce(0, (sum, p) -> sum += p.getAge(), (sum1, sum2) -> sum1 + sum2 );

        System.out.println(ageSum);



    }
}

