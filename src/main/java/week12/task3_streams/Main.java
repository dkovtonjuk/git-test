package week12.task3_streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> mylist = Arrays.asList("a1", "a2", "b1", "c2", "c1", "c2");

        mylist
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .distinct()
                .forEach(System.out::println);

        List<String> collected = mylist
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .limit(1)
                .distinct()
                .sorted()
                .collect(Collectors.toList());  //terminal operator

    }
}
