package week12;

@FunctionalInterface
interface IntegerToStringInterface {

    String intToString(int i);
}
