package week12.homeTraining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        IntStream.of(2, 3, 5, 5, 8, 8, 770)
                .filter(x -> x<300)
                .map(x -> x+11)
                .limit(4)    //gives only 4 first elements
                .forEach(System.out::println);

        /*   - filter — отбирает элементы, значение которых меньше 300,
             - map — прибавляет 11 к каждому числу,
             - limit — ограничивает количество элементов до 3.*/

        List<Integer> list = Arrays.asList(3, 4, 66, 7, 5);
        list
                .parallelStream()
                .filter(x -> x > 10)
                .map(x -> x * 2)
                .collect(Collectors.toList());


        IntStream.range(0, 10)
                .parallel()
                .map(x -> x*10)
                .sum();

        final List<Integer> ints = new ArrayList<>();
//        IntStream.range(0, 1000000)
 //               .parallel()
//                .forEach(i -> ints.add(i));
        System.out.println(ints.size());

        //Операторы Stream API:

/*empty stream*/
        Stream.empty()
                .forEach(System.out::println);

/* stream of values*/
Stream.of(1, 2, 3, 4, 55, 44)
        .forEach(System.out::println);

Stream.iterate(2, x -> x + 6)
        .limit(6)
        .forEach(System.out::print);


/*Объединяет два стрима так, что вначале идут элементы стрима A, а по его окончанию последуют элементы стрима B.*/
Stream.concat(
        Stream.of(1, 2, 3),
        Stream.of(8, 4, 3))
        .forEach(System.out::println);


/*builder() Создаёт мутабельный объект для добавления элементов в стрим без использования какого-либо контейнера для этого.*/

Stream.Builder<Integer> streamBuilder = Stream.<Integer>builder()
        .add(0)
        .add(1);
        for (int i = 2; i <= 8; i+=2 ) {
            streamBuilder.accept(i);
        }
        streamBuilder
                .add(0)
                .add(1)
                .build()
                .forEach(System.out::print);

        /*IntStream.range​(int startInclusive, int endExclusive) and  LongStream.range​(long startInclusive, long endExclusive)
Создаёт стрим из числового промежутка [start..end), то есть от start (включительно) по end.*/

        IntStream.range(0, 10)
                .forEach(System.out::print);

        LongStream.range(-10L, -5L)
                .forEach(System.out::print);

        /*IntStream.rangeClosed​(int startInclusive, int endInclusive)  and  LongStream.range​Closed(long startInclusive, long endInclusive)
Создаёт стрим из числового промежутка [start..end], то есть от start (включительно) по end (включительно).*/

        IntStream.rangeClosed(2, 10)
                .forEach(System.out::print);

        LongStream.range(3L, 12L)
                .forEach(System.out::print);


                            // Промежуточные операторы:

        /*filter​(Predicate predicate)     Фильтрует стрим, принимая только те элементы, которые удовлетворяют заданному условию.*/
        Stream.of(123, 342, 321, 123)
                .filter(x -> x > 100)
                .forEach(System.out::print);


}

}
