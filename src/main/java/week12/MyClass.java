package week12;

interface MyInterface {

    static int method(int i) {
        return i * 2;
    }

    default void defaultMethod(){
        System.out.println("bla bla");
    }  //we can write method body with "default"
}
