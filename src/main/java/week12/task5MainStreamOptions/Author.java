package week12.task5MainStreamOptions;

import java.util.Objects;

public class Author {

    Gender gender;
   Integer age;
   String authorNmame;

    public Author(String surname, Integer age, Gender gender) {
        this.gender = gender;
        this.age = age;
        this.authorNmame = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public String getAuthorName() {
        return authorNmame;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAuthorName(String surname) {
        this.authorNmame = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return gender == author.gender &&
                Objects.equals(age, author.age) &&
                Objects.equals(authorNmame, author.authorNmame);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gender, age, authorNmame);
    }
}
