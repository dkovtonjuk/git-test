package week12.task5MainStreamOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


public class Main {
    public static void main(String[] args) {
        List<Book> library = new ArrayList<>();

        library.add(new Book("title65", new Author("author1", 28, Gender.MALE)));
        library.add(new Book("title1", new Author("author2", 22, Gender.FEMALE)));
        library.add(new Book("title2", new Author("author3", 25, Gender.FEMALE)));
        library.add(new Book("title3", new Author("author1", 24, Gender.MALE)));
        library.add(new Book("title3", new Author("author8", 44, Gender.MALE)));
        library.add(new Book("title3", new Author("author6", 34, Gender.MALE)));

List<String> collected = library.stream()
        .map(book -> book.getAuthor())
        .filter(author -> author.getAge() >= 50)
        .map(Author::getAuthorName)
        .map(String::toUpperCase)
        .distinct()
        .limit(3)
      .collect(toList());



Map<Author, List<Book>> collect = library.stream().collect(Collectors.groupingBy(b -> b.getAuthor()));
        System.out.println(collect);

//        List <Human> humans = asList(
//                new Human ("Sam", asList("Buddy", "Lucy")),
//                new Human ("Bob", asList("Frankie", "Nina")));

//        List<String> petNames = humans.stream()
//                .map(Human::getPat)
//                .collect(Collectors.toList());




    }




}
