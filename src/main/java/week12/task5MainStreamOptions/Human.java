package week12.task5MainStreamOptions;

public class Human {
    private String name;
    private String pat;


    public Human(String name, String pat) {
        this.name = name;
        this.pat = pat;
    }

    public String getPat() {
        return pat;
    }

    public void setPat(String pat) {
        this.pat = pat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
