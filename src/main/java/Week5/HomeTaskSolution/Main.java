package Week5.HomeTaskSolution;

public class Main {
    public static void main(String[] args) {
        String s = "one";
        String s1 = s.concat("two");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("one ").append("two "); // append - add new symbol

      //  StringBuffer sinchronized and slower

        stringBuilder.append(1).reverse();  // add new symbol

        System.out.println(stringBuilder.toString());

        int a = 4; int b = 4;

        hardTask(a, b);
        hardTask(4, 5);
        hardTask(4, 6);
        hardTask(4, 7);
        hardTask(4, 8);
    }

//2 - Написать метод solution(int a, int b), который принимает на вход два числа, которые обозначают количество символов, которые должны быть напечатаны в итоговое строке (ЕСЛИ У НАС a - 4, b - 2, то у нас буква “a” должна встретится в итоговой строке 4 раза, а буква “b” 2 раза)
//    ВАЖНО: одинаковые символы не должны идти друг за другом 3 раза подряд
//    Примеры:
//    a: 1; b: 4; result: bbabb
//    a: 4; b: 4; result: bbaabbaa
//    a: 4; b: 5; result: bbaabbaab
//    a: 4; b: 6; result: bbaabbaabb
//    a: 4; b: 7; result: bbabbaabbab
//    a: 4; b: 8; result: bbabbabbaabb

    public static void hardTask(int a, int b){

        int tmp = 0;

        if (a > b) {tmp = a; a = b; b = tmp; }

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i <a+b; i++) {
          if (i < a) {
              stringBuilder.append("a");
          }
          if (i >= a) {
              stringBuilder.append("b");
          }
        }

        int f = 0;
        for (int i = 0; i < stringBuilder.length(); i = f) {

            StringBuilder stringBuilderTmp = new StringBuilder();

            for (int j = i; j < stringBuilder.length(); j++) {
                stringBuilderTmp.append(stringBuilder.charAt(j));
            }
            stringBuilder.replace(i, stringBuilder.length(), stringBuilderTmp.reverse().toString());
            if (f == 0) {
                f += 2;
            } else {
                if ((f % 2 != 0) | (a == b) | (a == b + 1) | (a == b + 2)) {
                    f += 2;
                } else {
                    if (f % 2 == 0) {
                        f++;
                    }
                }
            }
        }
            System.out.println("a: " + a + "; b: " + b + "; result: " + stringBuilder.toString());
        }
}

//        My result:
//        a: 4; b: 4; result: bbaabbaa
//        a: 4; b: 5; result: bbabbaaba
//        a: 4; b: 6; result: bbabbaabba
//        a: 4; b: 7; result: bbabbaabbab
//        a: 4; b: 8; result: bbabbaabbabb

// It was an interesting task.