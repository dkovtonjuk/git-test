package Week5.Calculator;

public class Calculator {

    //polimorphism

    int add(int a, int b) {
        return a + b;
    }

    int add(int a, int b, int c) {
        return a + b + c;
    }

}
