package Week5.Person;

public class Person  implements LivingCreature, Worker, Driver, Ukrainian{

    @Override
    public void breathe() {
        System.out.println("breathing");

    }

    @Override
    public void walk() {
        System.out.println("walking");
    }

    @Override
    public void work() {
        System.out.println("work");
    }

    @Override
    public void drive() {
        System.out.println("drive");
    }
    }

