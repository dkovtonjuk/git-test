package Week5.Person;

public class Main {
    public static void main(String[] args) {
        Person person = new Person();

        if (person instanceof Worker) {
            System.out.println("is worker");
        }

        if (person instanceof Driver) {
            System.out.println("is Driver");
        }

        if (person instanceof LivingCreature) {
            System.out.println("is living creature");
        }
        if (person instanceof Ukrainian) {
            System.out.println("is Ukrainian");
        }
    }
}
