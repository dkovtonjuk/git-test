package Week5.Person;

public interface LivingCreature {

    void walk();

    void breathe();
}
