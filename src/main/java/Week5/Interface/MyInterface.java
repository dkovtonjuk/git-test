package Week5.Interface;

public interface MyInterface {
    int myVariable = 1; //public static final

    void method1(); // public abstract void method1();

    void method2(); //public abstract int method2();
}
