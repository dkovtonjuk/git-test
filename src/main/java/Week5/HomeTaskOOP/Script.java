package Week5.HomeTaskOOP;

public class Script {
   private String scriptText;

    public Script (String scriptText) {
        this.scriptText = scriptText;
    }

    public String getScriptText(){

        return scriptText;
    }

    public void setScriptText(String scriptText){

        this.scriptText = scriptText;
    }
}
