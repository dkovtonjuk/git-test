package Week5.HomeTaskOOP;

public class CameraMan extends MovieCrewMember implements CameraUser{

    @Override
    public void useCamera() {
        System.out.println("I use camera SONY-2019 with new StarWars styled shape :-).");
    }

    public void participateInMovie(Movie name){

        System.out.print("My name is "+ getFirstName() +" "+ getLastName() + " adn I work with the camera. I am a fan of " + name.getName()+ ". ");
    }

}
