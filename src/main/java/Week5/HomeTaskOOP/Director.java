package Week5.HomeTaskOOP;

public class Director extends MovieCrewMember implements ScriptKnowledge{

  private   Script script;

  public Director (Script script) {
      this.script = script;
  }

    @Override
    void participateInMovie(Movie name) {
        System.out.println("My name is "+ getFirstName() +" "+ getLastName() + " adn I work with the script '" + getScript().getScriptText() + "of " + name.getName()+ ". The ganre of movie is "+ name.getGanre() + ". \nI hope we won`t get the Gold Raspberry\n ");
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    
}
