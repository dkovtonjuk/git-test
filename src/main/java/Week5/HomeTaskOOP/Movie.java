package Week5.HomeTaskOOP;

public class Movie {
    private String ganre;
   private String name;
    private Script script;

    public Movie (String name, String ganre, Script script) {
        this.name = name;
        this.ganre = ganre;
        this.script = script;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGanre() {
        return ganre;
    }

    public void setGanre(String ganre) {
        this.ganre = ganre;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

//public String getMovieName() {
//        return name;
//}
}

