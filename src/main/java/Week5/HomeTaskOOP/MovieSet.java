package Week5.HomeTaskOOP;

import java.util.ArrayList;

public class MovieSet {


    public static void main(String[] args) {

        Script starWarsGeneralVersion = new Script("Episode II 'ATTACK OF THE CLONES.....'");

        Script mainRoleVersion = new Script("Episode II 'ATTACK OF THE CLONES....'");

        Movie starWars = new Movie("Star Wars", "fiction", starWarsGeneralVersion);

        MovieCrewMember director = new Director(starWarsGeneralVersion);
        director.setFirstName("Bob");
        director.setLastName("Spielbeerg");

        MovieCrewMember actor = new Actor(true, mainRoleVersion);
        actor.setFirstName("Preddy");
        actor.setLastName("Pit");

        MovieCrewMember hotActress = new Actor(false,starWarsGeneralVersion);
        hotActress.setFirstName("Linda");
        hotActress.setLastName("Blondy");

        MovieCrewMember actressUnderstudy = new Actor(false, starWarsGeneralVersion);
        actressUnderstudy.setFirstName("MONIKA");
        actressUnderstudy.setLastName("Tolly");

        MovieCrewMember cameraUser = new CameraMan();
        cameraUser.setFirstName("Stiven");
        cameraUser.setLastName("Camerman");

        ArrayList<MovieCrewMember> movieCrew = new ArrayList<>();
        movieCrew.add(director);
        movieCrew.add(actor);
        movieCrew.add(hotActress);
        movieCrew.add(actressUnderstudy);
        movieCrew.add(cameraUser);


        for (int i = 0; i < movieCrew.size(); i++) {
            movieCrew.get(i).participateInMovie(starWars);

            if (movieCrew.get(i) instanceof CameraMan) {
                MovieCrewMember member = new CameraMan();
                ((CameraMan) member).useCamera();


                    }
                }


            }


        }

