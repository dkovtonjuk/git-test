package Week5.HomeTaskOOP;

public abstract class MovieCrewMember {
   private String firstName;
   private String lastName;



    public MovieCrewMember(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public MovieCrewMember(){}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    abstract void participateInMovie(Movie name);
}
