package Week5.HomeTaskOOP;


public class Actor extends MovieCrewMember implements ScriptKnowledge{


  private   boolean isMainRole;

  private   Script script;

public Actor (boolean isMainRole, Script script) {
    this.isMainRole = isMainRole;
    this.script = script;
}

    public Script getScript() {

    return script;
    }

    public void setScript(Script script) {

        this.script = script;
    }



    public boolean isMainRole() {
    return isMainRole;
    }

    public void setMainRole(boolean isMainRole) {
        this.isMainRole = isMainRole;
    }

    public void participateInMovie(Movie name) {
      if (isMainRole == true) {
        System.out.println("My name is " + getFirstName() + " " + getLastName() + " adn I will act with the script '" + getScript().getScriptText() + " of " + name.getName() + ".  I hope we will get the Oscar! \nI will do my best, because i have a main role.\n ");
      } else {
        System.out.println("My name is " + getFirstName() + " " + getLastName() + " adn I will act with the script '" + getScript().getScriptText() + " of " + name.getName() + ".  I hope we will get the Oscar!\n ");
      }
    }
}
