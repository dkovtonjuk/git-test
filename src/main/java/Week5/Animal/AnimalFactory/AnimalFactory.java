package Week5.Animal.AnimalFactory;


import Week5.Animal.Animal;
import Week5.Animal.Cat;
import Week5.Animal.Dog;
import Week5.Animal.Duck;

public class AnimalFactory {

        public Animal createAnimal(String type) {
            if (type.equalsIgnoreCase("cat")) {
                return new Cat();
            } else if (type.equalsIgnoreCase("dog")) {
                return new Dog();
            } else {if (type.equalsIgnoreCase("duck")) {
                return new Duck();
            } else { return null;
            }
            }
        }


//        public static void main(String[] args) {
//
//            Scanner scanner = new Scanner(System.in);
//            String argument = scanner.nextLine();
//
//            AnimalFactory animalFactory = new AnimalFactory();
//            Animal animal = animalFactory.createAnimal(argument);
//
//            if (animal != null) {
//                animal.makeNoise);
//            } else  {
//                System.out.println("boroda");
//            }


        }


//    }


