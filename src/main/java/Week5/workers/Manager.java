package Week5.workers;

public class Manager extends Worker {

    public Manager( String name, int salary, int totalExperience) {
        this.salary = salary;
        this.totalExperience = totalExperience;
        this.name = name;
    }

    public String toString() {
        return name + " " + salary + " $  and work for " + totalExperience;
    }


    public String managerName() {
        return name;
    }
}
