package Week5.workers;

public class Designer extends Worker {

public Designer( String name, int salary, int totalExperience) {
    this.salary = salary;
    this.totalExperience = totalExperience;
    this.name = name;
}
    public String designerName() {
        return name;
    }

    @Override
    public String toString() {
        return null;
    }
}
