package Week5.workers;

public class Spaceman  extends Worker{

    public Spaceman( String name, int salary, int totalExperience) {
        this.salary = salary;
        this.totalExperience = totalExperience;
        this.name = name;
    }

    public String toString() {
        return name + " " + salary + " $  and work for " + totalExperience;
    }



    public void goToSpace() {
        System.out.println("I amm going to space for" + salary + " $");
    }
}
