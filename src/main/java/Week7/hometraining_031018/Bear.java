package Week7.hometraining_031018;

import java.util.Objects;

public class Bear {
    private int weight;
    private int height;
    private String name;


 public Bear(int weight, int height, String name) {
    this.weight = weight;
    this. height = height;
    this.name = name;
}
 public int getWeight(){
     return weight;          }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
    return height;
    }

    public void setHeight(int height){
    this.height = height;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, height, name);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (o instanceof Bear) {
            Bear bearToCheck = (Bear) o;
            return bearToCheck.weight == this.weight && bearToCheck.height == this.height && bearToCheck.name.equals(this.name);
        }
return false;
    }

    @Override
    public String toString() {
        return name + " "+ weight + " " + height;
    }
}