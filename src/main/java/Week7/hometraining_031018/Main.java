package Week7.hometraining_031018;

import java.util.HashSet;

public class Main {
    public static void main(String[] args) {

        HashSet<Bear> bears = new HashSet<>();
        Bear bear = new Bear(230, 90, "berry");
        Bear bear2 = new Bear(230, 90, "berry");
        Bear bear3 = new Bear(230, 90, "berry1");
        Bear bear4 = new Bear(230, 90, "berry");
        bears.add(bear);
       bears.add(bear2);
       bears.add(bear3);
       bears.add(bear4);

        System.out.println(bears);
        System.out.println(bear2.equals(bear));
    }
}
