package Week7.hometraining_031018.hashMapExample;



import java.security.Key;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        HashMap<String, String> cityMap = new HashMap<>();
        cityMap.put("jon", "london");
        cityMap.put("bob", "kiev");
        cityMap.put("sem", "paris");
       countWords("mama mama mama papa mama");
        countWords1("mama mama mama papa mama mama");
    }


    public static HashMap<String, Integer> countWords(String text){
        String[] array = text.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        for (int i = 0; i < array.length ; i++) {
            Integer count = result.get(array[i]);
            if (result.containsKey(array[i])) {

              result.put(array[i], ++count);
            } else
            result.put(array[i], 1);
        }
        System.out.println(result);
        return result;
    }

    public static void countWords1(String text){
        String[] array = text.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        for (int i = 0; i < array.length ; i++) {
            Integer count = result.get(array[i]);
            if (result.containsKey(array[i])) {

                result.put(array[i], ++count);
            } else
                result.put(array[i], 1);
        }
//        Iterator<Map.Entry<String, Integer>> iter = result.entrySet().iterator();
//        while ( iter.hasNext()) {
//
//            System.out.println(iter.getKey() + iter.getValue());
//        iter.remove(); }


        for (Map.Entry<String, Integer> pair : result.entrySet()){
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }

    }

}
