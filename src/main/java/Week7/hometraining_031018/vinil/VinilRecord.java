package Week7.hometraining_031018.vinil;

import java.util.Objects;

public class VinilRecord {
    private int year;
    private String albumName;
    private String artist;

    VinilRecord(int year, String albumName, String artist){
        this.year = year;
        this.albumName = albumName;
        this.artist = artist;
    }
    public int getYear(){ return  year; }
    public void setYear(int year) {this.year = year; }
    public String getAlbumName() { return  albumName; }
    public void setAlbumName(String albumName) { this.albumName = albumName; }
    public String getArtist() {return  artist; }
    public void setArtist(String artist) { this.artist = artist; }

    @Override
    public String toString() {
        return year + " " + albumName+" "+artist;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, albumName, artist);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true; }
        if (o == null  || getClass() != o.getClass()) return  false;
        VinilRecord that = (VinilRecord) o;
        return year == that.year &&
                albumName.equals(that.albumName) &&
                artist.equals(that.artist);


    }
}
