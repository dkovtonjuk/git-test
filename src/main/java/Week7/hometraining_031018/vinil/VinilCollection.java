package Week7.hometraining_031018.vinil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;

public class VinilCollection {

   private HashSet<VinilRecord> collection = new HashSet<>();

   public void addToCollection(VinilRecord vinilrecord) throws VinilIsInCollectionException{

       for (int i = 0; i < collection.size(); i++) {

           if ( collection.contains(vinilrecord)) {
               throw new VinilIsInCollectionException();
           }
       }

       collection.add(vinilrecord);

       }
}
