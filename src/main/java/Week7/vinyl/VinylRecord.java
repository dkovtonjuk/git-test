package Week7.vinyl;

import java.util.Objects;

public class VinylRecord {
    private String nameOfVinill;
    private int yearOfProdusing;
    private String artist;

    public VinylRecord(String nameOfVinill, int yearOfProdusing, String artist){
        this.nameOfVinill = nameOfVinill;
        this.yearOfProdusing = yearOfProdusing;
        this.artist = artist;
    }

    public String getNameOfVinill() {
        return nameOfVinill;
    }

    public void setNameOfVinill(String nameOfVinill) {
        this.nameOfVinill = nameOfVinill;
    }

    public int getYearOfProdusing() {
        return yearOfProdusing;
    }

    public void setYearOfProdusing(int yearOfProdusing) {
        this.yearOfProdusing = yearOfProdusing;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VinylRecord that = (VinylRecord) o;
        return yearOfProdusing == that.yearOfProdusing &&
                Objects.equals(nameOfVinill, that.nameOfVinill) &&
                Objects.equals(artist, that.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfVinill, yearOfProdusing, artist);
    }
}

