package Week7;

import java.io.Serializable;
import java.util.Objects;

public class Bear  implements Serializable {
    private static final long serialVersionUID = 1L;
    private int weight;
    private int height;
    private String name;

    public Bear(int weight, int height, String name) {
        this.weight = weight;
        this.height = height;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return name +" "+ height + " " + weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bear bear = (Bear) o;
        return weight == bear.weight &&
                height == bear.height &&
                Objects.equals(name, bear.name);
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(weight, height, name);
//    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Bear bear = (Bear) o;
//        return weight == bear.weight &&
//                height == bear.height &&
//                Objects.equals(name, bear.name);
//    }
//
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + weight;
        hash = 31 * hash + height;
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        return hash;
    }
}
