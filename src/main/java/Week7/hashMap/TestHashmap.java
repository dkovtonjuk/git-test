package Week7.hashMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TestHashmap {
    public static void main(String[] args) {

        HashMap<String, String> userCiTyMap = new HashMap<String, String>();
        userCiTyMap.put("Jon", "NewJork");
        userCiTyMap.put("Jon", "New22");
        System.out.println(userCiTyMap.get("Jon"));
    }


    public static HashMap<String, Integer> countWords(String text) {
        String[] array = text.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            int count = 1;
            if ((array[i] != null) && !result.containsKey(array[i])) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[i].hashCode() == (array[j].hashCode())) {
                        count++;
                    }
                }
                result.put(array[i], count);
            }
        }
        return result;
    }
}