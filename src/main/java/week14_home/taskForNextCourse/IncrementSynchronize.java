package week14_home.taskForNextCourse;

import java.util.concurrent.atomic.AtomicInteger;

public class IncrementSynchronize {

    private int value = 0;

    AtomicInteger atomicInteger = new AtomicInteger();

    public synchronized int getNextValue(){
       return ++value;
    }


    public int getNextValue1(){
        value= atomicInteger .incrementAndGet();
        return ++value;
    }

    public int getNextValue2() {
       synchronized (this){
           return ++value;
       }
    }

    public static void main(String[] args) {
        IncrementSynchronize incremet = new IncrementSynchronize();
        System.out.println(incremet.getNextValue());
        System.out.println(incremet.getNextValue1());
        System.out.println(incremet.getNextValue2());
    }
    //get next value()
}
