package week14_home.taskForNextCourse;

import java.util.ArrayList;

public class FindDistinctWords {
    public static void main(String[] args) {

        // program for finding all distinct words

        findAllDistinctWordsFromText("Вони беруть участь ,,,,,у показах::::::;;; одягу від найпопулярніших дизайнерів, перебувають в центрі уваги сотні спалахів камер та демонструють своє життя тисячам людей, /які слідкують/ за ними в соцмережах. Хто вони – найуспішніші моделі світу, про яких знає кожен.");
    }


    public static void findAllDistinctWordsFromText(String text) {

        char[] symbols = {'/', ',', '.', '-', ';', ':', '–'};
        String[] array = text.toLowerCase().split(" ");

        ArrayList<String> list= new ArrayList<>( deleteChar(array, symbols));

        for(String b : list) {
            System.out.print(b + " ");
        }
    }

    public static ArrayList<String> deleteChar(String[] stringArr, char[] symbol) {

        for (int s = 0; s < symbol.length; s++) {

            for (int i = 0; i < stringArr.length; i++) {

                char[] charArr = stringArr[i].toCharArray();
                int symbolsInWord = 0;

                for (int j = 0; j < charArr.length; j++) {
                    if (charArr[j] == symbol[s]) {
                        {
                            symbolsInWord++;
                        }
                    }
                }

                char[] onlyLettersInCharArr = new char[stringArr[i].length() - symbolsInWord];
                int d = 0;

                for (int j = 0; j < stringArr[i].length(); j++) {

                    if (!(charArr[j] == symbol[s])) {
                        onlyLettersInCharArr[j - d] = charArr[j];
                    } else {
                        d++;
                    }
                }
                stringArr[i] = new String(onlyLettersInCharArr);
            }
        }

        ArrayList<String> strArrList = new ArrayList<>();
        for (int i = 0; i < stringArr.length; i++) {
            if (!stringArr[i].isEmpty()) {
                strArrList.add(stringArr[i]);}
        }


        return strArrList;
    }


}
