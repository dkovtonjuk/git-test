package week14_home;

import java.sql.*;

public class SelectExample {
    //JDBS driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/test";

    //Database credentials
    static final String USER = "root";
    static final String PASS = "dkov1234";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            stmt = conn.createStatement();

            String sql = "SELECT * FROM MUSIC_ALBUMS";

            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("ID");
                int releaseYear = rs.getInt("RELEASE_YEAR");
                String name = rs.getString("NAME");
                String artist = rs.getString("ARTIST");
                String genre = rs.getString("GENRE");

                System.out.println(id + " " + releaseYear + " " + name + " "
                + artist + " " + genre);
            }

            System.out.println("Selected all records successfully");

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception se) {
            //Handle errors for Class.forName
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }
}
