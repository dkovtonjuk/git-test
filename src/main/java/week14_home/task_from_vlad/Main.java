package week14_home.task_from_vlad;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

       System.out.println(stringPoints("man i need a taxi up to ubud"));

    }

    public static String stringPoints(String text) {
        int data = 0;
        int points = 0;
        String result = null;

        String[] array = text.split(" ");

        for (int i = 0; i < array.length; i++) {

            char[] chars = array[i].toLowerCase().toCharArray();
            for (int j = 0; j < chars.length; j++) {

                points += chars[j] - 'a' + 1; }

            if (points > data) {
                data = points;
                result = array[i];
            }
            points = 0;
        }
        return result;
    }


}
