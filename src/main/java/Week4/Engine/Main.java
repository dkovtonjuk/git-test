package Week4.Engine;

import Week5.workers.Designer;
import Week5.workers.Manager;
import Week5.workers.Programmer;

public class Main {
    public static void main(String[] args) {
        CombustEngine combustEngineForMoto = new CombustEngine("combustEngineForMoto",40, false);
        CombustEngine combustEngineTruck = new CombustEngine("combustEngineTruck", 220, false);
        ElectricEngine electricEngine = new ElectricEngine("electricEngine",160, true);


        Programmer programmer = new Programmer("programmer Rob", 200, 2);
        String programmerName = programmer.prorraName();
        Manager manager = new Manager("manager Bob", 200, 3);
        String managerName = manager.managerName();
        Designer designer = new Designer("designer Emma", 230, 3);
        String designerName = designer.designerName();



        ElectricCar electricCar = new ElectricCar("electricCar",5, 250, 4, electricEngine, programmerName, 320);
        Motocycle motocycle = new Motocycle("motocycle",2, 200, 2, combustEngineForMoto, managerName);
        Truck truck = new Truck("truck", 4, 140, 4, combustEngineTruck, designerName);

        Vehicle[] vehicles = new Vehicle[3];
        vehicles[0] = electricCar;
        vehicles[1] = motocycle;
        vehicles[2] = truck;

        for (int i = 0; i <vehicles.length; i++) {
            System.out.println(vehicles[i].toString());
        }

        Vehicle ifVehicleIsFriendly = new Vehicle();
        ifVehicleIsFriendly.ifEngineFriendlyForEnvironment(vehicles);
    }


}
