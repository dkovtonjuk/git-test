package Week4.Engine;

public class Engine {
    String name;
    int power;
    boolean isEnvFriendly;

    public Engine(){}

    public Engine( String name, int power, boolean isEnvFriendly) {
        this.name = name;
        this.power = power;
        this.isEnvFriendly = isEnvFriendly;
    }
}
