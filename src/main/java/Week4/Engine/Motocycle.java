package Week4.Engine;

public class Motocycle extends Vehicle{

    CombustEngine combustEngineForMoto = new CombustEngine("combustEngineForMoto",40, false);

    public Motocycle  (String name, int numberOfSeats, int speed, int numberOfWheel, Engine engineOfWhicle, String owner) {
        this.name = name;
        this.numberOfSeats =   numberOfSeats;
        this.speed = speed;
        this.numberOfWheel = numberOfWheel;
        this.engineOfWhicle = engineOfWhicle;
        this.owner = owner;

    }

    public String toString() {
        return name + " has " + numberOfSeats + " seets; speed is "+ speed + "; wheels: " + numberOfWheel + "; engineType: " + engineOfWhicle.name + "; owner: " + owner;
    }
}
