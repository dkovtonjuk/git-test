package Week4.Engine;

public class ElectricCar extends Vehicle{

    int range;
    ElectricEngine electricEngine = new ElectricEngine("electricEngine", 200, true);



    public ElectricCar  (String name, int numberOfSeats, int speed, int numberOfWheel, Engine engineOfWhicle, String owner, int range) {
       this.name = name;
       this.numberOfSeats =   numberOfSeats;
        this.speed = speed;
        this.numberOfWheel = numberOfWheel;
        this.engineOfWhicle = engineOfWhicle;
        this.owner = owner;
        this.range = range;

    }


    public String toString() {
        return name + " has " + numberOfSeats + " seets; speed is "+ speed + "; wheels: " + numberOfWheel + "; engineType: " + engineOfWhicle.name + "; owner: " + owner + "; range: "+ range;
    }
}
