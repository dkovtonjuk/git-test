package Week4.Engine;

public class Vehicle {
    String name;
    int numberOfSeats;
    int speed;
    int numberOfWheel;
    Engine engineOfWhicle;
//    boolean engineOfWhicle;
    String owner;

    public Vehicle() {
    }

    public Vehicle(int numberOfSeats, int speed, int numberOfWheel, Engine engineOfWhicle, String owner) {
        this.numberOfSeats = numberOfSeats;
        this.speed = speed;
        this.numberOfWheel = numberOfWheel;
        this.engineOfWhicle = engineOfWhicle;
        this.owner = owner;

    }

    public String engineNameInfo(){
        return name;
    }

    public void ifEngineFriendlyForEnvironment(Vehicle[] vehicles) {
        for (int i = 0; i < vehicles.length; i++) {
            if (vehicles[i].engineOfWhicle.isEnvFriendly == true) {
                System.out.println("This " + vehicles[i].name + " is friendly for environment :)");
            } else {
                System.out.println("This " + vehicles[i].name + " is NOT friendly for environment :(");

            }
        }
    }
}
