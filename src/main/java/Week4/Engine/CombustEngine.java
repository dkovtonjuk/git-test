package Week4.Engine;

public class CombustEngine extends Engine{



    public CombustEngine (String name, int power, boolean isEnvFriendly) {
        this.name = name;
        this.power = power;
        this.isEnvFriendly = isEnvFriendly;
    }

    public boolean isFriendly(){
        return isEnvFriendly;
    }
    }

