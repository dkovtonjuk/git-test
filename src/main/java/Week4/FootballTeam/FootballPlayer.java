package Week4.FootballTeam;

public class FootballPlayer {
    String name;
    int transferValue;

    public FootballPlayer() {}

    public FootballPlayer(String name, int transferValue){
        this.name = name;
        this.transferValue = transferValue;
    }

    public void play(){

        System.out.println("I play football for "+ transferValue + " because my name is " + name);
    }

}
