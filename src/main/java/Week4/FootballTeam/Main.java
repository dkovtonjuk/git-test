package Week4.FootballTeam;

public class Main {

    public static void main(String[] args) {
    LeftFullBack leftFullBack = new LeftFullBack("Zinadin Zidane", 99999);
    RightFullBack rightFullBack = new RightFullBack("Andrij Gusin", 88888);
    CentralFullback centralFullback = new CentralFullback("Alex Ronaldo", 98000);
    CentralHalfBack centralHalfBack = new CentralHalfBack("Sergij Rebrov", 57000);
    LeftHalfBack leftHalfBack = new LeftHalfBack("Andrij Shevchenko", 39099);
    RightHalfBack rightHalfBack = new RightHalfBack("Pele", 44440);
    SecondCentralHalfBack secondCentralHalfBack = new SecondCentralHalfBack("Zozulija", 87000);
    LeftForward leftForward = new LeftForward("Ronaldinjo", 39994);
    InsideForward insideForward = new InsideForward("Konoplyanka", 88799);
    RightForward rightForward = new RightForward("Maurinjo", 99988);
    Goalkeeper goalkeeper = new Goalkeeper("A. Shovkovskij", 49998, true);

    FootballPlayer[] footballTeam= new FootballPlayer[11];
        footballTeam[0] = leftFullBack;
        footballTeam[1] = centralFullback;
        footballTeam[2] = rightFullBack;
        footballTeam[3] = centralHalfBack;
        footballTeam[4] = leftHalfBack;
        footballTeam[5] = rightHalfBack;
        footballTeam[6] = secondCentralHalfBack;
        footballTeam[7] = leftForward;
        footballTeam[8] = insideForward;
        footballTeam[9] = rightForward;
        footballTeam[10] = goalkeeper;

        for (int i = 0; i < footballTeam.length ; i++) {
            footballTeam[i].play();
        }
    }
}
