package Week4.FootballTeam;

public class Goalkeeper extends FootballPlayer {

    boolean isAbleToPlayByHands = true;

    public Goalkeeper(){}

    public Goalkeeper(String name, int transferValue, boolean isAbleToPlayByHands) {
        this.name = name;
        this.transferValue = transferValue;
        this.isAbleToPlayByHands = isAbleToPlayByHands;
    }

    public void play(){

        System.out.println("I play with hands in football for "+ transferValue + " because my name is " + name);
    }

}
