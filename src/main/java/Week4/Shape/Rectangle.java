package Week4.Shape;

public class Rectangle  extends Shape{
   private double length;
    private double width;

    public double getLength(){
        return length;
    }

    public void setLength(double length){
        this.length = length;
    }

    public double getWidth(){
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }
    @Override
    public double area() {
        return length*width;
    }
    @Override
    public double perimeter() {
        return 2*(length+width);
    }
}
