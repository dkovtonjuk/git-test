package Week4.Shape;

public class Triangle extends Shape{

    double botton;
    double b;
    double c;

    public Triangle( double botton, double b, double c){
        this.botton = botton;
        this.b = b;
        this.c = c;
    }
    @Override
    public double area() {
        double halfPerimetr = (botton+b+c)/2;
        return Math.sqrt(halfPerimetr*(halfPerimetr-botton)*(halfPerimetr-b)*(halfPerimetr-c));
    }
    @Override
    public double perimeter() {
        return botton+b+c;
    }
}

