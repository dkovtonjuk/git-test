package Week4.Shape;

public class Main {

    public static void main(String[] args) {

        double botton = 5.0, b = 4.0, c =3.0;
        Shape triangle = new Triangle(botton, b, c);

        System.out.println("\n"+"Triangle area: "+ triangle.area() + " Triangle perimetr: " + triangle.perimeter()+"\n");

        double length = 5, width = 7;
        Rectangle rectangle = new Rectangle(length, width);
        System.out.println("Rectangle area: "+ rectangle.area() + "  Rectangle perimetr: " + rectangle.perimeter()+"\n");

        double radius = 5;
        Shape circle = new Circle(radius);
        System.out.println(((Circle) circle).getRadius());
        System.out.println("Circle area: "+circle.area() + "  Circle perimetr: "+ circle.perimeter()+"\n");


    }
}
