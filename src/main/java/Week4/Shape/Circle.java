package Week4.Shape;

public class Circle extends Shape {

    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius){
        this.radius = radius;
    }

    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius, 2);
    }
@Override
    public double perimeter() {
        return 2*Math.PI*radius;
    }
}
