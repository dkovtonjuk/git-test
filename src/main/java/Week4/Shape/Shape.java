package Week4.Shape;

public abstract class Shape {


   // public Shape(){}

    public abstract double area();

    public abstract double perimeter();
}
