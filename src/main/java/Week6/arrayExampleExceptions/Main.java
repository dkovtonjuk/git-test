package Week6.arrayExampleExceptions;

import Week6.arrayExampleExceptions.exception.ListIsFullException;

public class Main {
    public static void main(String[] args) {


        MyList list = new MyList(5);

        try {
            list.add(2);
            list.add(2);
            list.add(2);
            list.add(3);
            list.add(2);
            list.add(2);
        } catch (ListIsFullException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(list);
    }
}
