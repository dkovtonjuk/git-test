package Week6.arrayExampleExceptions;

import Week6.arrayExampleExceptions.exception.ListIsFullException;

import java.util.Arrays;

public class MyList {

    private int[] array;
    private int currentIndex;

    MyList(int size) {
        this.array = new int[size];
        this.currentIndex = 0;
    }

    public void add(int arg) throws ListIsFullException{

      if (currentIndex == array.length) {
          throw  new ListIsFullException(array.length);
      }
            array[currentIndex++] = arg;



    }



    @Override
    public String toString() {return Arrays.toString(array); }



    }

