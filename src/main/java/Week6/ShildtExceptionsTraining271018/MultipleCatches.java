package Week6.ShildtExceptionsTraining271018;

public class MultipleCatches {
    public static void main(String[] args) {
        try {
            int a = 12;
            System.out.println("a: " + a);
            int b = 42/a;
            int[] c = {1};
            c[42] = 99;
        } catch(ArithmeticException e) {
            System.out.println("division by zero: "+e);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("mistake with indexation out of array: "+e);
        }
        System.out.println("after try / catch blocks");
    }
}
