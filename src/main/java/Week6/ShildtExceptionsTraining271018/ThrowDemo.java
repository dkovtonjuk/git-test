package Week6.ShildtExceptionsTraining271018;

public class ThrowDemo {
    static void demoproc() {
        try {
            throw new NullPointerException("demonstration");
        } catch (NullPointerException e) {
            System.out.println("Exception has been cought in demoproc() method");
            throw e; //repeat throwing the exception
        }
    }

    public static void main(String[] args) {
        try {
            demoproc();
        } catch (NullPointerException e) {
            System.out.println("repeated catch. " + e);
        }
    }
}
