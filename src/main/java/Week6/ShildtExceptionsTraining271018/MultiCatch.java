package Week6.ShildtExceptionsTraining271018;

public class MultiCatch {
    public static void main(String[] args) {
        int a = 10, b = 0;
        int vals[] = {1, 2, 3};

        try {
           // int result = a / b;
            vals[10] = 19;
        } catch(ArithmeticException | IndexOutOfBoundsException e) {
            System.out.println("the exception hads been cought: " + e);
        }
        System.out.println("after multi catch");
    }
}
