package Week6.ShildtExceptionsTraining271018;

public class ThrowsDemo {
    static void throwOne() throws IllegalAccessException {
        System.out.println("in the throwOne() method");
        throw new IllegalAccessException("demonstration");
    }

    public static void main(String[] args) {
        try {
            throwOne();
        } catch (IllegalAccessException e) {
            System.out.println("The Exception has been cought: " + e);
        }
    }
}
