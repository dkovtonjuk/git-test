package Week6.ShildtExceptionsTraining271018;

public class ChainExcDemo {
    static void demoproc() {

        NullPointerException e = new NullPointerException("High level");
        e.initCause(new ArithmeticException("the reason"));
        throw e;
    }

    public static void main(String[] args) {
        try {
            demoproc();
        } catch (NullPointerException e) {
            System.out.println("the exception has been caught: " + e);
            System.out.println("the first reason: " + e.getCause());
        }
    }
}
