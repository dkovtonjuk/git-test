package Week6.ShildtExceptionsTraining271018;

public class FinallyDemo {
    static void procA() {
        try {
            System.out.println("in the procA() body");
            throw new RuntimeException("demonstration");
        } finally {
            System.out.println("finally operator block in procA() method");
        }
    }

    static  void procB() {
        try {
            System.out.println("in the procB() body");
            return;
        } finally {
            System.out.println("finally operator block in procB() method");
        }
    }

    static void procC() {
        try {
            System.out.println("in the procC body");

        } finally {
            System.out.println("finally operator block in procC() method");
        }
    }

    public static void main(String[] args) {
        try {
            procA();
        } catch (Exception e) {
            System.out.println("Exception has been caught");
        }
        procB();
        procC();
    }
}
