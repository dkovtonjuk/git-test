package week14;

import java.sql.*;

public class ConnectExample {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/test";

    static final String USER = "debian-sys-maint";
    static final String PASS = "WRzCJlfJxL4egSpV";

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            stmt = conn.createStatement();

            String sql = "SELECT * FROM MUSIC_ALBUMS";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println(rs);

            while (rs.next()) {
                int id = rs.getInt("ID");
                int releaseYear = rs.getInt("RELEASE_YEAR");
                String name = rs.getString("NAME");
                String artist = rs.getString("ARTIST");
                String genre = rs.getString("GENRE");

                System.out.println(id+ " " + releaseYear + " " + name + " " + artist + " " + genre );
            }

//            String sql = "INSERT INTO MUSIC_ALBUMS (ID, NAME, RELEASE_YEAR, ARTIST, GENRE)\n" +
//                    "VALUES (12, 'Curtis', 2015, 'Curtis Mayfield', 'soul');";
//
//            stmt.executeUpdate(sql);
//
//            System.out.println("Inserted record successfully");

        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }
}
