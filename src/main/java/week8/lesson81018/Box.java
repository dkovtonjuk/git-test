package week8.lesson81018;

public class Box<T> {

    private T t;

    public Box(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    public static void main(String[] args) {
        Box<String> stringBox = new Box<>("I am string");


        Box<Integer> integerBox = new Box<>(22);
        System.out.println(stringBox.getT());
        System.out.println(integerBox.getT());
    }
}
