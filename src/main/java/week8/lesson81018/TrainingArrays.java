package week8.lesson81018;

import java.util.HashSet;

public class TrainingArrays {


    public static void main(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5};
        int[] array2 = {2, 3, 4, 5, 6, 7};
        System.out.println(crossArrays(array1, array2));
        System.out.println(crossArraysThroughHashSet(array1, array2));
        System.out.println(crossArraysThroughHashSet2(array1, array2));
    }

    public static int crossArrays(int[] array1, int[] array2) {

        int counter = 0;
        for (int i = 0; i < array1.length; i++) {

            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    public static int crossArraysThroughHashSet(int[] array1, int[] array2) {
        HashSet<Integer> hashSet1 = new HashSet<>();
        HashSet<Integer> hashSet2 = new HashSet<>();

        for (int i = 0; i < array1.length; i++) {
            hashSet1.add(array1[i]);
        }
        for (int i = 0; i < array2.length; i++) {
            hashSet2.add(array2[i]);
        }

        hashSet1.retainAll(hashSet2);

        return hashSet1.size();

    }

    public static boolean crossArraysThroughHashSet2(int[] array1, int[] array2) {
        HashSet<Integer> hashSet1 = new HashSet<>();
        HashSet<Integer> hashSet2 = new HashSet<>();

        for (int i = 0; i < array1.length; i++) {
            hashSet1.add(array1[i]);
        }
        for (int i = 0; i < array2.length; i++) {
            hashSet2.add(array2[i]);
        }

        return hashSet2.retainAll(hashSet1);

    }


}
