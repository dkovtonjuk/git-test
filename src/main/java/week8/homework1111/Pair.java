package week8.homework1111;

public class Pair <T, E> {
  private  T t;
  private E e;

  public Pair(T t, E e) {
      this.t = t;
      this.e = e;
  }

    public E getE() {
        return e;
    }

    public void setE(E e) {
        this.e = e;
    }

    public T getT() {
      return t;
    }

    public void setT(T t) {
      this.t = t;
    }

    @Override
    public String toString() {
      return
              "( " + t + " " + e + " )";
    }

    public Integer otherT () {

    Integer r = null;
    if (t instanceof Integer) {
        r = (Integer) t + 55;
        }
        return (Integer) (t = (T) r);
}

    public String tGetClass () {

        return t.getClass().toString();
    }

    public void otherE() {
    if (e instanceof String ) {
        e = (E) ((String) e).concat(" ").concat((String) e);
    }
       }


    public String eGetClass () {

        return e.getClass().toString();
    }

}
