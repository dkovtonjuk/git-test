package week8;

import java.util.Objects;

public class Book implements Comparable<Book>{

    private  int pages;

    public Book(int pages) {
        this.pages = pages;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return pages == book.pages;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pages);
    }

    @Override
    public int compareTo(Book that) {
        if (this.pages == that.pages) {
            return 0;
        }
        if (this.pages < that.pages) {
            return -1;
        } else {
        return 1; }
    }

    @Override
    public String toString() {
        return "pages of book "+ pages;
    }
}
