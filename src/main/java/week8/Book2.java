package week8;

import Week5.Task2.C;

import java.util.Objects;

public class Book2 implements Comparable<Book2>{
    private int pages;
    private String author;

    public Book2(int pages, String author) {
        this.pages = pages;
        this.author = author;
    }

    public String getAuthor() { return author; }

    public void setAuthor(String pages) { this.author = author; }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book2 book2 = (Book2) o;
        return pages == book2.pages &&
                Objects.equals(author, book2.author);
    }

    @Override
    public String toString() {
        return author+ " " + pages ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pages, author);
    }


    @Override
    public int compareTo(Book2 that) {
        int compared = this.author.compareTo(that.author);

        if (compared != 0) {
            return compared;
        }

        compared = Integer.compare(this.pages, that.pages);
        return compared;
    }
}
