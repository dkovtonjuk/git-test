package week8.vinil2;

import java.util.Objects;

public class Vinil2 implements Comparable<Vinil2>{
        private String nameOfVinill;
        private int yearOfProdusing;
        private String artist;

        public Vinil2(String nameOfVinill, int yearOfProdusing, String artist){
            this.nameOfVinill = nameOfVinill;
            this.yearOfProdusing = yearOfProdusing;
            this.artist = artist;
        }

        public String getNameOfVinill() {
            return nameOfVinill;
        }

        public void setNameOfVinill(String nameOfVinill) {
            this.nameOfVinill = nameOfVinill;
        }

        public int getYearOfProdusing() {
            return yearOfProdusing;
        }

        public void setYearOfProdusing(int yearOfProdusing) {
            this.yearOfProdusing = yearOfProdusing;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vinil2 vinil2 = (Vinil2) o;
        return yearOfProdusing == vinil2.yearOfProdusing &&
                Objects.equals(nameOfVinill, vinil2.nameOfVinill) &&
                Objects.equals(artist, vinil2.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfVinill, yearOfProdusing, artist);
    }

    @Override
    public String toString() {
        return artist + " " +nameOfVinill+ " " + yearOfProdusing ;
    }

    @Override
    public int compareTo(Vinil2 that) {
        int compared = this.artist.compareTo(that.artist);

        if (compared != 0) {
            return compared; //if we could to sort
        }

        compared = Integer.compare(this.nameOfVinill.hashCode(), that.nameOfVinill.hashCode());

        compared = Integer.compare(this.yearOfProdusing, that.yearOfProdusing);

        return compared;
    }
}

