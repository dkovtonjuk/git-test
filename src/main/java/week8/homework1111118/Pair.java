package week8.homework1111118;

public class Pair <T, E> {
  private  T t;
  private E e;

  public Pair(T t, E e) {
      this.t = t;
      this.e = e;
  }

    public E getE() {
        return e;
    }



    public T getT() {
      return t;
    }



    @Override
    public String toString() {
      return
              e.getClass().toString() + " " + t.getClass().toString();
    }

//    public Integer otherT () {
//
//    Integer r = null;
//    if (t instanceof Integer) {
//        r = (Integer) t + 55;
//        }
//        return (Integer) (t = (T) r);
//}
//
//    public String tGetClass () {
//
//        return t.getClass().getName();
//    }
//
//    public void otherE() {
//    if (e instanceof String ) {
//        e = (E) ((String) e).concat(" ").concat((String) e);
//    }
//       }
//
//
//    public String eGetClass () {
//
////        return e.getClass().toString();
//    }

}
