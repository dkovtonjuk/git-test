package Week3;

import java.util.Arrays;

public class Homework071018 {

    public static void main(String[] args) {
        System.out.println(alphabetNumbers("Conor McGregor"));
    }
//Метод, который получает на вход строку и возвращает массив интов, которое отображают порядковый номер каждой  буквы в алфавите (пробелы или какие то символы можно заменять на -1), алфавит английский

//    Пример:
//
//    alphabetNumbers("Conor McGregor") → [3, 15, 14, 15, 18, -1, 13, 3, 7, 18, 5, 7, 15, 18]

    public static String alphabetNumbers(String str) {
        String strToLower = str.toLowerCase();
        char[] arr = strToLower.toCharArray();
        char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        int[] numbers = new int[arr.length];
        int count = 0;

      for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < alphabet.length; j++) {

                if (arr[i] == alphabet[j]) {
                    numbers[count] = j + 1;
                    count++; break;
                }
            }
                if(count!=i+1){
                    numbers[count] = -1;
                    count++;
                }
       }
        return Arrays.toString(numbers);
    }
    }

