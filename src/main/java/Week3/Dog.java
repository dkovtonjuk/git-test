package Week3;

public class Dog {
    String breed;
    int age;
    String colour;
    int size;
    boolean isAllive;

    public Dog(){ }

    public Dog(String breed, int age, String colour, int size, boolean isAllive){
        this.breed = breed;
        this.age = age;
        this.colour = colour;
        this.size = size;
        this.isAllive = isAllive;
    }

    public  void getDogInfo(){
        System.out.println(breed + " " + age + " " + colour + " "+ size+ " " + isAllive);
    }

}
