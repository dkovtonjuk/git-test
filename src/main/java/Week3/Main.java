package Week3;

public class Main {
    public static void main(String[] args) {
        Dog retriever = new Dog("retrivier", 67, "white", 22, true);
        Dog mongrel = new Dog("mongrel", 21, "different", 19, true);
        Dog neapolianMastiff = new Dog("neapolinmastiff", 4, "red", 6, true);
        Dog maltese = new Dog("maltese", 12, "dark", 23, true);
        Dog chowChow = new Dog("chowchow", 11, "red", 25, true);


        Dog[] dogs = new Dog[5];
        dogs[0] = retriever;
        dogs[1] = neapolianMastiff;
        dogs[2] = maltese;
        dogs[3] = chowChow;
        dogs[4] = mongrel;

        System.out.println("before..");
        for (int i = 0; i < dogs.length; i++) {
            dogs[i].getDogInfo();
        }

        DogService dogService = new DogService();
        dogService.killMongels(dogs);

        System.out.println("after...");
        for (int i = 0; i < dogs.length; i++) {
            dogs[i].getDogInfo(); }

//            for (int i = 0; i < dogs.length; i++) {
//                System.out.println(dogs[i].isAllive);
//            }


        }
    }

