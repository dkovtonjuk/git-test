package Week3.Training101018;

public class Apple {
    String colour;
    int weight;
    String region;

    public Apple() {}

    public Apple(String colour, int weight, String region){
        this.colour = colour;
        this.weight = weight;
        this.region = region;
    }

    public void getAppleInfo(Apple[] apples){
        System.out.println(colour + " " + weight + " " + region);
    }
}

