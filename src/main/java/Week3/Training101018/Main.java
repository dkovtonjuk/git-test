package Week3.Training101018;

public class Main {
    public static void main(String[] args) {
        Apple newSort = new Apple("red", 200, "Odessa");
        Apple antonovka = new Apple("yellow", 190, "Lviv");
        Apple naliv = new Apple("white", 120, "Ukraine");
        Apple golden = new Apple("yellow", 180, "Lviv");

        Apple[] apples = new Apple[4];
       apples[0] = newSort;
       apples[1] = antonovka;
       apples[2] = naliv;
       apples[3] = golden;

        System.out.println("before");

       for (int i = 0; i < apples.length; i++) {
          apples[i].getAppleInfo(apples);
        }

        System.out.println("after");

SortApples sort1 = new SortApples();
sort1.sortApplesByColour(apples);

        for (int i = 0; i < apples.length; i++) {
            apples[i].getAppleInfo(apples);
        }

        System.out.println("after oneStepRight");

        sort1.oneStepRight(apples);

        for (int i = 0; i < apples.length; i++) {
            apples[i].getAppleInfo(apples);
        }

        System.out.println("after takeOne");
        sort1.takeOneApple(apples);

        for (int i = 0; i < apples.length; i++) {
            apples[i].getAppleInfo(apples);
        }

    }
}
