package Week3.Training101018;

public class SortApples {

    public void sortApplesByColour(Apple[] apples){
        for (int i = 0; i < apples.length-1; i++) {
          int alphabetIndex = (int) apples[i].colour.charAt(0);
            int alphabetIndexNext = (int) apples[i+1].colour.charAt(0);
            if(alphabetIndexNext<=alphabetIndex){
                Apple tmp = apples[i];
                apples[i] = apples[i+1];
                apples[i+1] = tmp;
            }
        }
    }

    public void oneStepRight(Apple[] apples){

        for (int i = apples.length-1; i >apples.length-2; i--) {
           Apple startElement = apples[i];
            for (int j = apples.length-1; j > 0; j--) {
                apples[j] = apples[j-1];
            }
            apples[0] = startElement;
            }
    }

    public void takeOneApple(Apple[] apples){


        for (int i = 0; i <apples.length; i++) {
         if(apples[i].colour.equals("yellow")){
             apples[i].colour = "     ";
         }
        }

    }
}
