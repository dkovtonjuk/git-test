package testTask.configuration;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DataToFileWriter {

    public static void writeResultAverageMinutesToFile(ArrayList<Integer> list) throws IOException {
        FileOutputStream outputStream = new FileOutputStream("/home/dmytro/IdeaProjects/elementary-course/gittest/src/main/java/testTask/files/input.txt");

        for (int i = list.size() - 1; i >= 0; i--) {
            if(list.get(i)!=0){
            byte[] strToBytes = String.valueOf(list.get(i)).getBytes();
            outputStream.write(strToBytes);
            }else {
                byte[] strToBytes = ("-").getBytes();
                outputStream.write(strToBytes);
            }

            outputStream.write('\n');
        }
    }

}
