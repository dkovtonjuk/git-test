package testTask.configuration;

import java.io.File;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class DataFromFileReader {

    public static ArrayList<String> readLinesFromFileAndReturnArrayList() throws FileNotFoundException {
        final ArrayList<String> stringsFromFile = new ArrayList<>();
        Scanner scanner = new Scanner(new File("/home/dmytro/IdeaProjects/elementary-course/gittest/src/main/java/testTask/files/input.txt"));
        while (scanner.hasNext()) {
            stringsFromFile.add(scanner.nextLine());
        }
        return stringsFromFile;
    }

   }
