package testTask.service;


import entity.C;
import entity.D;
import testTask.configuration.DataToFileWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static testTask.service.DataToMapSaver.mapWithObjects;

public class DataAnalizer {

    public static void analizeDataFromResultMap() throws IOException {
        ArrayList result = new ArrayList<Integer>();
        for (Map.Entry<D, ArrayList<C>> item : mapWithObjects.entrySet()) {
            int minutes = 0;
            int count = 0;
            D d = item.getKey();
            ArrayList<C> listC = item.getValue();
            for (int j = 0; j < listC.size(); j++) {
                C c = listC.get(j);
                if (c.getServiceId() != d.getServiceId() & d.getServiceId() != '*') {
                    continue;
                }
                if (c.getServiceVariationId() != d.getServiceVariationId() & d.getServiceVariationId() != 0) {
                    continue;
                }
                if (c.getQuestionTypeId() != d.getQuestionTypeId() & d.getQuestionTypeId() != '*' & d.getQuestionTypeId() != 0) {
                    continue;
                }
                if (c.getQuestionCategoryId() != d.getQuestionCategoryId() & d.getQuestionCategoryId() == '*' & d.getQuestionTypeId() != 0) {
                    continue;
                }
                if (c.getResponseType() != d.getResponseType()) {
                    continue;
                }
                if (!c.getDate().after(d.getDateFrom()) | !c.getDate().before(d.getDateTo())) {
                    continue;
                }
                count++;
                minutes += c.getMinutes();
            }
            if (minutes != 0) {
                result.add(minutes / count);
            } else {
                result.add(0);
            }
        }
        DataToFileWriter.writeResultAverageMinutesToFile(result);
    }

}


