package service;

import entity.C;
import entity.D;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ObjectFromStringGenerator {
    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public static C generateC(String string) throws ParseException {
        String[] components = string.split(" ");
        C c = new C('C', 0, 0, 0, 0, 0, ' ', new Date(), 0);

        String[] id = components[1].split("\\.");
        c.setServiceId(Integer.valueOf(id[0]));
        if (id.length > 1)
            c.setServiceVariationId(Integer.valueOf(id[1]));

        String[] question = components[2].split("\\.");
        c.setQuestionTypeId(Integer.valueOf(question[0]));
        if (question.length > 1)
            c.setQuestionCategoryId(Integer.valueOf(question[1]));
        if (question.length > 2)
            c.setQuestionSubCategoryId(Integer.valueOf(question[2]));
        c.setResponseType(components[3].charAt(0));

        Date date = format.parse(components[4]);
        c.setDate(date);

        c.setMinutes(Integer.valueOf(components[5]));

        return c;
    }


    public static D generateD(String string) throws ParseException {
        String[] components = string.split(" ");
        D d = new D('D', 0, 0, 0, 0, 0, ' ', new Date(), new Date());

        String[] id = components[1].split("\\.");
        if (id[0].charAt(0) != '*')
            d.setServiceId(Integer.valueOf(id[0]));

        if (id.length > 1 && id[1].charAt(0) != '*')
            d.setServiceVariationId(Integer.valueOf(id[1]));

        String[] question = components[2].split("\\.");
        if (question[0].charAt(0) != '*')
            d.setQuestionTypeId(Integer.valueOf(question[0]));

        if (question.length > 1 && question[1].charAt(0) != '*')
            d.setServiceVariationId(Integer.valueOf(question[1]));

        if (question.length > 2 && question[2].charAt(0) != '*')
            d.setServiceVariationId(Integer.valueOf(question[2]));

        d.setResponseType(components[3].charAt(0));

        String[] dateInString = components[4].split("-");
        if (dateInString.length > 0) {
            Date date = format.parse(dateInString[0]);
            d.setDateFrom(date);
        }
        if (dateInString.length > 1) {
            Date date1 = format.parse(dateInString[1]);
            d.setDateTo(date1);
        } else {
            Date date1 = format.parse("00.00.0000");
            d.setDateTo(date1);
        }
        return d;
    }
}
