package testTask.service;


import entity.C;
import entity.D;
import testTask.configuration.DataFromFileReader;

import java.io.*;
import java.text.*;
import java.util.*;

public class DataToMapSaver {
    public static final HashMap<D, ArrayList<C>> mapWithObjects = new HashMap<>();


    public static void saveLinesToMap() throws ParseException, FileNotFoundException {
        ArrayList<String>stringsFromFile = DataFromFileReader.readLinesFromFileAndReturnArrayList();
        ArrayList<C> listC = new ArrayList<C>();
        for (int i = 1; i < stringsFromFile.size(); i++) {
            if (stringsFromFile.get(i).charAt(0) == 'C') {
                listC.add(service.ObjectFromStringGenerator.generateC(stringsFromFile.get(i)));
            }
            if (stringsFromFile.get(i).charAt(0) == 'D') {
                ArrayList<C> list = new ArrayList<C>();
                list.addAll(listC);
                mapWithObjects.put(service.ObjectFromStringGenerator.generateD(stringsFromFile.get(i)), list);
            }
        }
    }

}
