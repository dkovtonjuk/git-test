package testTask;

import testTask.service.DataAnalizer;
import testTask.service.DataToMapSaver;

import java.io.IOException;
import java.text.ParseException;


public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        DataToMapSaver.saveLinesToMap();
        DataAnalizer.analizeDataFromResultMap();
    }
}
