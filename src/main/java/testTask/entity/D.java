package entity;

import java.util.Date;

public class D {
/* D service_id[.variation_id] question_type_id[.category_id.[sub-category_id]] P/N date_from[-date_to] */
    char type;
    int serviceId;
    int serviceVariationId;
    int questionTypeId;
    int questionCategoryId;
    int questionSubCategoryId;
    char responseType;
    Date dateFrom;
    Date dateTo;

    public D() {
    }

    public D(char type, int serviceId, int serviceVariationId, int questionTypeId, int questionCategoryId, int questionSubCategoryId, char responseType, Date dateFrom, Date dateTo) {
        this.type = type;
        this.serviceId = serviceId;
        this.serviceVariationId = serviceVariationId;
        this.questionTypeId = questionTypeId;
        this.questionCategoryId = questionCategoryId;
        this.questionSubCategoryId = questionSubCategoryId;
        this.responseType = responseType;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getServiceVariationId() {
        return serviceVariationId;
    }

    public void setServiceVariationId(int serviceVariationId) {
        this.serviceVariationId = serviceVariationId;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public int getQuestionCategoryId() {
        return questionCategoryId;
    }

    public void setQuestionCategoryId(int questionCategoryId) {
        this.questionCategoryId = questionCategoryId;
    }

    public int getQuestionSubCategoryId() {
        return questionSubCategoryId;
    }

    public void setQuestionSubCategoryId(int questionSubCategoryId) {
        this.questionSubCategoryId = questionSubCategoryId;
    }

    public char getResponseType() {
        return responseType;
    }

    public void setResponseType(char responseType) {
        this.responseType = responseType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public int hashCode() {
        int result = (int) getType();
        result = 31 * result + getServiceId();
        result = 31 * result + getServiceVariationId();
        result = 31 * result + getQuestionTypeId();
        result = 31 * result + getQuestionCategoryId();
        result = 31 * result + getQuestionSubCategoryId();
        result = 31 * result + (int) getResponseType();
        result = 31 * result + getDateFrom().hashCode();
        result = 31 * result + getDateTo().hashCode();
        return result;
    }

}
