package Week6_7.lesson221018.factorial;

public class Factorial {

    public static void main(String[] args) {
     exampleOfFactorial(0, 1);

    }

    public static void factorial(){
        int result = 1;
        for (int i = 1; i <6; i++) {
            result *= i;
        }
        System.out.println("fori factorial is: " + result);
    }

    public static int recursiveFactorial(int count){
        if (count <= 1) {return 1; }

        return count*recursiveFactorial(count-1);
    }

    public static  void exampleOfFactorial(int n, int count){
        System.out.println(n);
        n = n * n;
        n++;
        count++;
        if (count > 5) {return ; }
        exampleOfFactorial(n, count);
    }
}
