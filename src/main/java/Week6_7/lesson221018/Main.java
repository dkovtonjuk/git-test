package Week6_7.lesson221018;



        public class Main {

            public static void main(String[] args) {
                System.out.println(solution(4, 2));
            }

            static String solution(int a, int b) {
                StringBuilder resultString = new StringBuilder();

                while (a > 0 || b > 0) {
                    if (a > b) {
                        if (canAppend(resultString, 'a')) {
                            resultString.append('a');
                            a--;

                        } else {
                            resultString.append('b');
                            b--;
                        }
                    } else {
                        if (canAppend(resultString, 'b')) {
                            resultString.append('b');
                            b--;

                        } else {
                            resultString.append('a');
                            a--;
                        }
                    }
                }
                return resultString.toString();
            }

            private static boolean canAppend(StringBuilder str, char toCheck) {
                if (str.length() < 2) {
                    return true;
                }
                return (str.charAt(str.length() - 1) != toCheck) || (str.charAt(str.length() - 2) != toCheck);
            }
        }


