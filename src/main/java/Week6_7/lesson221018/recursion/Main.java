package Week6_7.lesson221018.recursion;

public class Main {

    public static void main(String[] args) {
example(5);
    }

    static void example(int times) {

        times--;
        System.out.println(times + " times left");
        if (times == 0) {
            return;
        }

        example(times);

    }
}
