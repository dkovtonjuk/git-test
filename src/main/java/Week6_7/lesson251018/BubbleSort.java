package Week6_7.lesson251018;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
       int[] array = {3, 1, 234534, 1, 55, 6} ;
       bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }

    static void bubbleSort(int array[]) {

        for (int i = 0; i < array.length - 1 ; i++) {
            for (int j = 0; j < array.length - i - 1 ; j++) {

                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }

        }
    }
}
