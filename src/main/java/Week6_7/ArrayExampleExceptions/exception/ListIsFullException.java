package Week6_7.ArrayExampleExceptions.exception;

public class ListIsFullException extends Exception{

    private int length;

    public ListIsFullException(int length) {
        this.length = length;
    }

    public String getMessage() {
        return "List is already full + (length is " + this.length + ")";
    }

}
