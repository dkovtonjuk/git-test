package Week6_7.recursion1;

public class Main {
    public static void main(String[] args) {
example(12);
    }

    //example of recursion
    static void example(int increment){

        increment++;

        if (increment == 5) {
            return;
        }
        example(increment);  // exit from the method
    }
}
