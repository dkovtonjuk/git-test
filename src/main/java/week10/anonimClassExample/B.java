package week10.anonimClassExample;

import java.util.ArrayList;
import java.util.List;

public class B {

    void method(){

    }

    public static void main(String[] args) {

        B b = new B(){
            @Override
            void method()  {
                System.out.println("overrided");
            }
        };

        b.method();

        List<String> list = new ArrayList<String>() {{
            add("www"); add("wxxd");
        }};
    }
}
