package week10.reflection;



import Week7.hometraining_031018.Bear;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Bear bear = new Bear(23, 32, "Big Bear");

        Class clazz = bear.getClass();
  // System.out.println( clazz.getName());

Field[] fields = clazz.getFields();

        for(Field f : fields) {
            System.out.println(f.getName());
        }

       Method[] methods = clazz.getMethods();

for (Method m : methods) {
    System.out.println(m.getName());
}
    }
}
