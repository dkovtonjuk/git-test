package week10.reflection3;

import week10.reflection1.MyClass;

import java.lang.reflect.Field;

public class ReflectBear {
    public static void main(String[] args) {
        Bear bear = new Bear(22, 33, "dvd");
        Class bearClass = bear.getClass();
        String nameValue;

        try {
            Field field = bear.getClass().getDeclaredField("name");
            field.setAccessible(true);
            field.set(bear, (String) "new value");

            nameValue = (String) field.get(bear);
            System.out.println(nameValue);

            field = bear.getClass().getDeclaredField("weight");
            field.setAccessible(true);
            field.set(bear, (int) 22);

            int newWeight = (int) field.get(bear);
            System.out.println(newWeight);

            field = bear.getClass().getDeclaredField("height");
            field.setAccessible(true);
            field.set(bear, (int) 36);

            int newHeight = (int) field.get(bear);
            System.out.println(newHeight);


        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

//        try {
//            Field field = bear.getClass().getDeclaredField("weight");
//            field.setAccessible(true);
//            field.set(bear, (int) 22);
//
//            int newWeight = (int) field.get(bear);
//            System.out.println(newWeight);
//
//        } catch (NoSuchFieldException | IllegalAccessException e) {
//            e.printStackTrace();
//        }
    }
}
