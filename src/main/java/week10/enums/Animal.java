package week10.enums;

public enum Animal {
    CAT {
        public String makeNoise() {
            return "maoy!";
        }
    },
    DOG {
        public String makeNoise() {
            return "woof!";
        }
    };

    public abstract String makeNoise();

    public static void main(String[] args) {
        System.out.println(Animal.CAT.makeNoise());
    }

}