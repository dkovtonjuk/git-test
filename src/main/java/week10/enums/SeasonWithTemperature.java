package week10.enums;

public enum SeasonWithTemperature {
    WINTER(0), SPRING(10), SUMMER(25), AUTUMN(10);

    private final  int temperature;

    SeasonWithTemperature(int temperature) {
        this.temperature = temperature;
    }

    public static void main(String[] args) {
        SeasonWithTemperature autumn = SeasonWithTemperature.AUTUMN;
        System.out.println(autumn.temperature);

        for (SeasonWithTemperature season : SeasonWithTemperature.values()) {
            System.out.println(season.temperature);
        }
    }
}
