package week10.enums.DvdState;

import static week10.enums.DvdState.DvdState.*;

public class DvdController {

    private DvdPlayer player = new DvdPlayer();

    public void processRequest(int satusCode) {

           switch (satusCode){
            case 1:
                player.setState(ON);
                System.out.println("DVD player is on");
break;
            case 2:
                player.setState(OFF);
                System.out.println("DVD player is off");
break;
            case 3:
                player.setState(STANDBY);
                System.out.println("DVD player in standby mode");
break;
                default:
                    System.out.println("can not process");
    }

        }
    }

