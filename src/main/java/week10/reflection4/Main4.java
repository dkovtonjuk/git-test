package week10.reflection4;

import week10.reflection1.MyClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main4 {
    public static void main(String[] args) {
        MyClass myClass = null;

        try {
            Method method = myClass.getClass().getDeclaredMethod("printData"); //call class without object of class
            method.setAccessible(true);
            method.invoke(myClass); //call method

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
