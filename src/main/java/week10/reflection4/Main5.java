package week10.reflection4;

import week10.reflection1.MyClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main5 {
    public static void main(String[] args) {
        MyClass myClass = null;

        try {
            Class clazz = Class.forName(MyClass.class.getName());
            myClass = (MyClass) clazz.newInstance();

        } catch (IllegalAccessException |  ClassNotFoundException | InstantiationException e) {
            e.printStackTrace();
        }
        System.out.println(myClass);
    }
}
