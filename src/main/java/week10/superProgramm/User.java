package week10.superProgramm;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userName;
    private String password;
    private String eMail;
    private String name;

    public User(String userName, String password, String eMail, String name) {
        this.userName = userName;
        this.password = password;
        this.eMail = eMail;
        this.name = name;
    }

    @Override
    public String toString() {
        return userName + " " + password + " " + eMail + " " + name;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, eMail, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User that = (User) o;
        return
                Objects.equals(userName, that.userName) &&
                        Objects.equals(password, that.password) &&
                        Objects.equals(eMail, that.eMail) &&
                        Objects.equals(name, that.name);
    }
}
