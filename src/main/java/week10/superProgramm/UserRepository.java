package week10.superProgramm;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static week10.superProgramm.UserCreateClass.createUser;

public class UserRepository {

    void userManagement() {
        HashSet<User> users = new HashSet<>(getUserFromFile(new File(Constants.PATH + Constants.FOLDER_PATH + Constants.FILE_PATH)));
        User user = createUser();

        createFolder(Constants.PATH + Constants.FOLDER_PATH);
        File file = createFile(Constants.PATH + Constants.FOLDER_PATH+ Constants.FILE_PATH);

        boolean userExist = checkIfUserExists(users, user);
        if (userExist == true) {
            System.out.println(Constants.USER_EXIST_MESSAGE);
            userManagement();
        } else {
            saveUser(users, user, file);
            System.out.println(Constants.NEW_USER_MESSAGE);
        }

    }


private void createFolder(String path) {
        File file = new File(path);
        if(!file.exists()) {
            if (!file.mkdir()) {
                System.out.println("folder was not created");
            }
        }
}


private File createFile (String path){
        File file;
    file = new File(path);

    return file;
}


    private void saveUser(HashSet<User> users, User user, File file) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))){
            if (!users.contains(user))
            users.add(user);
            for (User d : users) {
                System.out.println(d);
            }
            objectOutputStream.writeObject(users);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIfUserExists( HashSet users, User user){
        boolean result = true;

if (!users.contains(user)) { result = false; }
        return result;
    }

    private static Set<User> getUserFromFile(File file) {
        Set<User> users = new HashSet<>();
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {

            users = (Set<User>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
return users;
    }
}
