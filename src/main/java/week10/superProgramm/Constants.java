package week10.superProgramm;

public class Constants {
    public static final String USERNAME_REGEX = "[a-z0-9A-Z]{3,16}$";
    public static final String ENTER_USERNAME = "enter username";
    public static final String ERROR_MESSAGE_USERNAME = "You have typed wrong symbols. Try Again! (a-z, length : 3-16)";

    public static final String PASSWORD_REGEX = "[a-z0-9_-]{6,18}$";
    public static final String ENTER_PASSWORD= "enter password";
    public static final String ERROR_MESSAGE_PASSWORD= "You have typed wrong symbols. Try Again! (a-z, 0-9, -, _, length : 6-16)";


    public static final String EMAIL_REGEX = "([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$";
    public static final String ENTER_EMAIL= "enter email";
    public static final String ERROR_MESSAGE_EMAIL= "Wrong  e-mail format. Try Again! (a-z, 0-9, -, _, @...)";


    public static final String NAME_REGEX = "([A-Za-z\\. -]+)";
    public static final String ENTER_NAME= "enter Name and Surname";
    public static final String ERROR_MESSAGE_NAME= "Wrong  Name format. Try Again! (A-Z, a-z, -, space, ...)";

    public static final String PATH = "/home/dmytro/IdeaProjects/gittest/src/main/java/week10/superProgramm";
    public static final String FOLDER_PATH = "/usersFolder";
    public static final String FILE_PATH = "/users.txt/";


    public static final String USER_EXIST_MESSAGE = "user with mentioned fields exists!";
    public static final String NEW_USER_MESSAGE = "your registration is succesful!";



}
