package week10.superProgramm;

import java.util.Scanner;

public class UserCreate {


    public static User createUser () {
        User user = null;

Scanner scanner = new Scanner(System.in);

        String userName = readWithRetry(scanner, Constants.USERNAME_REGEX, Constants.ENTER_USERNAME, Constants.ERROR_MESSAGE_USERNAME);
        String password = readWithRetry(scanner, Constants.PASSWORD_REGEX, Constants.ENTER_PASSWORD, Constants.ERROR_MESSAGE_PASSWORD);
        String eMail = readWithRetry(scanner, Constants.EMAIL_REGEX, Constants.ENTER_EMAIL, Constants.ERROR_MESSAGE_EMAIL );
        String name = readWithRetry(scanner, Constants.NAME_REGEX, Constants.ENTER_NAME, Constants.ERROR_MESSAGE_NAME);
        user =  new User(userName, password, eMail, name);
        return  user;

    }



    private static String readWithRetry(Scanner scanner, String regex, String messageToShow, String errorMessage) {
        while (true) {
            System.out.println(messageToShow);
            String input = scanner.nextLine();
            if (input.matches(regex)) {
                return input;
            } else {
                System.out.println(errorMessage);
            }

        }
    }
}
