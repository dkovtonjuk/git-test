package week10.regularExpression;

import java.util.Scanner;

public class StringVariant {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String number = scanner.nextLine();

            if(number.equals("stop")) break;

            if (number.matches("-?\\d+"))  System.out.println("the number is " + number);

            if (!number.matches("-?\\d+")) System.out.println("wrong number");



        }
        System.out.println("-123".matches("-?\\d+"));


        System.out.println("123".matches("-?\\d+"));
        System.out.println("+123".matches("-?\\d+"));
        System.out.println("+123".matches("(-|\\+)?\\d+"));
    }
}
