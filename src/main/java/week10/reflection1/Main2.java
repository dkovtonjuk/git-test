package week10.reflection1;

import java.lang.reflect.Field;

public class Main2 {
    public static void main(String[] args)  {
        MyClass myclass = new MyClass();
        String nameValue;

        try {
            Field field = myclass.getClass().getDeclaredField("name");
            field.setAccessible(true);
            nameValue = (String) field.get(myclass);
            System.out.println(nameValue);

            field.set(myclass, (String) "newValue");
            nameValue = (String) field.get(myclass);
            System.out.println(nameValue);


        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //field.set(myclass, (String) "new walue");
    }
}
