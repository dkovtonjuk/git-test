package week10.airplaneClassInClass;

public class Airplane {
    private String name, id, flight;

    private Wing leftWing = new Wing("red", "23"), rightWing= new Wing ("blue", "25");

    public Airplane(){

    }

    public Airplane (String name, String id, String flight) {
        this.name = name;
        this.id = id;
        this.flight = flight;
    }

    private class Wing {
        private String colour, model;

        public Wing(String colour, String s) {
            this.colour = colour;
            this.model = model;
        }
    }

    public static void main(String[] args) {
        Airplane airplane = new Airplane();
        airplane.new Wing("white", "long");

        Wing wing = new Airplane().new Wing("red", "1");
    }
}
