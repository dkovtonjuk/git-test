package week9.laptop;

import week9.shpping.ShoppingItem;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class InventoryManager {


    public void makeInventoryFileByPrices(String inventoryFilepath) {
        Set<Laptop> laptops = getLaptopSetFromFile(new File(inventoryFilepath));
        createFolder("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/laptop/laptopsFolder/");
        File over1500LaptopsFile = createFile("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/laptop/laptopsFolder/over1500");
        File under1500LaptopsFile = createFile("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/laptop/laptopsFolder/under1500");
        analizeLaptopAndSaveToFiles(laptops, over1500LaptopsFile, under1500LaptopsFile);
    }

    public static Set<Laptop> getLaptopSetFromFile(File file) {
        Set<Laptop> laptops = new HashSet<Laptop>();
        try {

            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(":");

                String name = lineScanner.next();
                String size = lineScanner.next();
                String prise = lineScanner.next();

                Laptop laptop = new Laptop(name, Integer.parseInt(size), Integer.parseInt(prise));

                laptops.add(laptop);
                try {
                    laptops.add(laptop);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return laptops;
    }

    public static void createFolder(String path) {
        File file = new File(path);
        if (!file.exists()) {
            if (file.mkdir()) ;
        } else {
            System.out.println("the folder wasn`t created");
        }
    }


    public File createFile(String path) {
        File file = new File(path);
        return file;
    }

    public void analizeLaptopAndSaveToFiles(Set<Laptop> laptops, File over1500LaptopsFile, File under1500LaptopsFile) {

        for (Laptop d : laptops) {
            if (d.getPrise() < 1500) {
                System.out.println("Under1500: " + d.toString());
                try (ObjectOutputStream objectOutputStreamUnder1500 = new ObjectOutputStream(new FileOutputStream(under1500LaptopsFile))) {

                    objectOutputStreamUnder1500.writeObject(new Laptop(d.getName(), d.getSize(), d.getPrise()));
                    objectOutputStreamUnder1500.write('\n');

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                System.out.println("Over1500: " + d.toString());
                try (ObjectOutputStream objectOutputStreamOver1500 = new ObjectOutputStream(new FileOutputStream(over1500LaptopsFile))) {

                    objectOutputStreamOver1500.writeObject(new Laptop(d.getName(), d.getSize(), d.getPrise()));
                    objectOutputStreamOver1500.write('\n');

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
