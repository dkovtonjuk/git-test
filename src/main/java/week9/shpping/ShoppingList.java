package week9.shpping;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ShoppingList {

    private Set<ShoppingItem> items = new HashSet<ShoppingItem>();

    public Set<ShoppingItem> getList() {
        return items;
    }

    public void parseItem(String fileName) throws FileNotFoundException {

        try {

            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(":");

               String name = lineScanner.next();
               String quantity = lineScanner.next();

ShoppingItem item = new ShoppingItem(name, Integer.valueOf(quantity));

items.add(item);

            }

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
