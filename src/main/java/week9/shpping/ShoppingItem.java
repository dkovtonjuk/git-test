package week9.shpping;

import java.util.Objects;

public class ShoppingItem {
    private String name;
    private Integer quantity;

    public ShoppingItem(String productName, Integer quantity) {
        this.name = productName;
        this.quantity = quantity;
    }

    public String getProductName() {
        return name;
    }

    public Integer getQuantity() {
        return quantity;
           }

    public void setProductName(String productName) {
        this.name = productName;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return name + "=" + quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingItem that = (ShoppingItem) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, quantity);
    }
}
