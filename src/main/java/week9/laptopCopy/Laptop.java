package week9.laptopCopy;

import java.io.Serializable;

public class Laptop implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private int size;
    private int prise;

    public Laptop(String name, int size, int prise) {
        this.name = name;
        this.size = size;
        this.prise = prise;
            }

     public String getName() {
        return name;
    }
     public void setName (){
        this.name = name;
    }

    public int getSize(){
        return size;
    }

    public void setSize() {
        this.size = size;
    }

    public int getPrise() {
        return prise;
    }

    public void setPrise() {
        this.prise = prise;
    }

    @Override
    public String toString() {
        return name + "|" + size + "|"+  prise;
    }
}
