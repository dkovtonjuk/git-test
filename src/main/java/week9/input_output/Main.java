package week9.input_output;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        inputoutput();
    }

    static void inputoutput() {
       // String line = null;
        try(Scanner scanner = new Scanner(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/input_output/read.txt"))) {
            while (scanner.hasNext()) {
               String line = scanner.nextLine();

            OutputStream output = new FileOutputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/input_output/write.txt"), true);
            byte[] bytes = line.getBytes();
            output.write(bytes); }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
