package week9.output;



import Week7.Bear;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//output_1();
//        output_5();
        lessonTaskWithObjects();
      // output_3();
    //    output_2();
       // output_4();
     //   surrealisable();
    }

    static void output_1() {
        try (OutputStream output = new FileOutputStream(new File("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/output/file.txt"), true)) {

            String text = "hello";
            byte[] bytes = text.getBytes();
            output.write(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output_2() {
try (OutputStream output = new BufferedOutputStream(new FileOutputStream(new File ("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/output2.txt")))) {

    String text = "hello!";
    byte[] bytes = text.getBytes();
    output.write(bytes);

} catch (FileNotFoundException e) {
    e.printStackTrace();
} catch (IOException e) {
    e.printStackTrace();
}
    }


    static void output_3() {
        try(OutputStream output = new FileOutputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/output3_txt"))) {

            PrintStream printStream = new PrintStream(output);
            printStream.println("some text 2");
            printStream.println("some text 3");
            printStream.print(14);
            System.out.println("file is written");

            System.setOut(printStream);
            System.out.println("lkfghjk");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output_4 () {
        try (Writer writer = new FileWriter("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/output4")){
writer.write("hello world writer!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output_5() {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/output/file1.txt")))) {
            Person person = new Person("vlad", 23, 230, false);
            output.writeObject(person);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void output_6() {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/file3.txt")))) {
          Person person = new Person("vlad", 44, 55, false)  ;
          output.writeObject(person);

          ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/file3.txt")));
              Person person1 = (Person) input.readObject();
            System.out.println(person1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void lessonTaskWithObjects() {

        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/writeBear.txt"), true))) {
            ArrayList<Bear> bears = new ArrayList<>();
            Bear bear1 = new Bear(11,24, "koll");
            Bear bear2 = new Bear(13,23, "r4rfll");
            Bear bear3 = new Bear(14,22, "kogg");
            bears.add(bear1);
            bears.add(bear2);
            bears.add(bear3);

            output.writeObject(bears);

            ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/writeBear.txt")));
            ArrayList<Bear> bears2= (ArrayList<Bear>) input.readObject();
            System.out.println(Arrays.toString(new ArrayList[]{bears2}));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}