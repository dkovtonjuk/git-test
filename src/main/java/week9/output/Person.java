package week9.output;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    private String name;
    private int namew;
    private int nt;
    private boolean rr;

    public Person(String name, int namew, int nt, boolean rr) {


        this.name = name;
        this.namew = namew;
        this.nt = nt;
        this.rr = rr;
    }

    @Override
    public String toString() {
        return name + " " + namew + " " + nt + " " + rr;
    }
}
