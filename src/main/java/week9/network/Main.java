package week9.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = null;
        try {
            URL url = new URL("https://pitchfork.com/reviews/albums/j-mascis-elastic-days/");
            InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
            br = new BufferedReader(inputStreamReader);

            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {

                sb.append(line);
                sb.append(System.lineSeparator());
            }

            System.out.println(sb);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {

                br.close();
            }
        }
    }
}
