package week9.io;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
//read1();
//read2();
//read3();
        System.out.println(Arrays.toString(read5()));
    }

    static void read1() {

        InputStream input = null;


        try {
            input = new FileInputStream("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/io/test.txt");
       int data = input.read();
       while ( data != -1){
            data = input.read();
            System.out.println(data); }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close(); 
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void read2() {
        try (InputStream input = new FileInputStream("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/io/test.txt")) {
            int availableBytes = input.available();
            byte[] bytes = new byte[availableBytes];

            input.read(bytes);
            String s = new String(bytes, StandardCharsets.UTF_8);
            System.out.println(Arrays.toString(bytes));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void read4() throws IOException {
       try ( BufferedReader br = new BufferedReader(new FileReader((new File("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/io/test.txt"))))) {
        String st;
        while ((st = br.readLine())!= null) {
            System.out.println(st);
        }
        }catch (IOException e) {
           e.printStackTrace();
       }
    }


    static String[] read5() throws IOException {
        String[] array = new String[5];
        int iterator = 0;
        try ( BufferedReader br = new BufferedReader(new FileReader((new File("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/io/test.txt"))))) {
            String st;

            while ((st = br.readLine())!= null) {
                array[iterator] = st;
                iterator++;
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }
}

