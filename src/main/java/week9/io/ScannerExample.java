package week9.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) throws FileNotFoundException {
        example();
        example2();
    }

    static void example() throws FileNotFoundException {
        Scanner scanner;
        scanner = new Scanner(new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/io/test.txt"));

        while ( scanner.hasNext()) {
            System.out.println(scanner.nextLine());
        }
    }

    static void example2() throws FileNotFoundException {
        try {
            Scanner scanner = new Scanner(new File ("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/io/split"));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(",");

                while (lineScanner.hasNext()) {
                    String part = lineScanner.next();
                    Integer integer = Integer.valueOf(part);
                    System.out.println(integer);
                }
                System.out.println();
            }

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
