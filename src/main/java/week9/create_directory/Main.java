package week9.create_directory;

import javax.imageio.IIOException;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

      //  dir_file();
       // file1();
        dir1();
    }

    static void file1() {
        File file = new File ("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/create_directory/fileFromFile1.txt");
        if (!file.exists()){
            try{
                if(file.createNewFile()) {
                    System.out.println("directory is created");
                } else {
                    System.out.println("failed to create directory");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void dir1() {
        File file = new File("/home/dmytro/IdeaProjects/gittest/src/main/java/week9/output/create_directory/11folder/");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("directory created");
            } else {
                System.out.println("failed to create directory");
            }
        }
    }

    static void dir_file() {
        File file = new File("/home/kevinpc3/IdeaProjects/gittest/src/main/java/week9/output/create_directory/11.txt/");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("directory is created");
                String folderPath = file.getAbsolutePath();
                String filePath = folderPath + "/" + "myfile.txt";
                System.out.println(filePath);
                File file1 = new File(filePath);
                try {
                    file1.createNewFile();
                } catch (IIOException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
                }

            } else  {
                System.out.println("failed to create directory");
            }
    }
}
}
