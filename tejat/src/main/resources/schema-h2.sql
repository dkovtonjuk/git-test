CREATE TABLE IF NOT EXISTS RAILCARS1  (
  ID INT AUTO_INCREMENT PRIMARY KEY ,
  TYPE VARCHAR(255) NOT NULL,
  CAPASITY INTEGER(16) NOT NULL,
  PRODUCTIONYEAR INTEGER(16) NOT NULL
) ;

CREATE TABLE IF NOT EXISTS passenger(
            id INT AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR (255) NOT NULL,
            email VARCHAR (255) NOT NULL,
            phoneNumber INTEGER(16),
            trainNumber INTEGER(16),
            seatNumber INTEGER(16) );

CREATE TABLE IF NOT EXISTS train(
            id INT AUTO_INCREMENT PRIMARY KEY,
            number VARCHAR (255) NOT NULL,
            model VARCHAR (255) NOT NULL,
            departure_station VARCHAR (255),
            arrival_station VARCHAR (255)
            );


--
-- CREATE TABLE IF NOT EXISTS victim  (
--   ID INT AUTO_INCREMENT PRIMARY KEY,
--   lastName VARCHAR(40) NOT NULL,
--   name VARCHAR(40),
--   patronymic VARCHAR(100),
--   adress VARCHAR(255),
--   telefon VARCHAR(100),
--   comment VARCHAR(255),
--   eventId BIGINT
-- ) ;