
<!DOCTYPE html>
<html lang="en">
<head>
    <title>add railcar</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update railcar</h2>
    <p>Do it!</p>
    <form action="${pageContext.request.contextPath}/railcar/update/save" method="post">
        <fieldset>
            <legend>${dataString}_${object.getId()}</legend>
            <input type="hidden" name="id" value="${object.getId()}"/><br/><br/>
            Type : <input type="text" name="type" value="${object.getType()}"/><br/><br/>
            Capacity : <input type="number" name="capacity" value="${object.getCapasity()}"/><br/><br/>
            ProdYear : <input type="number" name="prodyear" value="${object.getProductionYear()}"/><br/><br/>
        </fieldset>
        <input type="submit" name="add" value="Save" />
    </form>

</div>

</body>
</html>