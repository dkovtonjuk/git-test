
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Choose one</title>
</head>
<body>


<h2>RailCar:</h2>

<form action="${pageContext.request.contextPath}/railcar" method="get">
    <input type="submit" name="getall" value="GetAll" />
</form>

<form action="${pageContext.request.contextPath}/addrailcar.jsp">
    <input type="submit" name="saveone" value="SaveOne" />
</form>

<div>
<form action="${pageContext.request.contextPath}/railcar/update" method="post">
    <fieldset>
        <legend>Update railcar</legend>
        ID : <input type="number" name="id" value="${object.getId()}"/>
    </fieldset>
    <input type="submit" name="upd" value="Update"/>
</form>
</div>

<div>
    <form action="${pageContext.request.contextPath}/railcar/delete" method="get">
        <fieldset>
            <legend>Delete railcar</legend>
            ID : <input type="number" name="id" value="${object.getId()}"/>
        </fieldset>
        <input type="submit" name="del" value="Delete"/>
    </form>

</div>





<h2>Passenger:</h2>

<form action="${pageContext.request.contextPath}/passenger" method="get">
    <input type="submit" name="getall" value="GetAll" />
</form>

<form action="${pageContext.request.contextPath}/addpassenger.jsp">
    <input type="submit" name="saveone" value="SaveOne" />
</form>

<div>
    <form action="${pageContext.request.contextPath}/passenger/update" method="post">
        <fieldset>
            <legend>Update passenger</legend>
            ID : <input type="number" name="id" value="${object.getId()}"/>
        </fieldset>
        <input type="submit" name="upd" value="Update"/>
    </form>
</div>

<div>
    <form action="${pageContext.request.contextPath}/passenger/delete" method="get">
        <fieldset>
            <legend>Delete passenger</legend>
            ID : <input type="number" name="id" value="${object.getId()}"/>
        </fieldset>
        <input type="submit" name="del" value="Delete"/>
    </form>

</div>



<h2>Train:</h2>

<form action="${pageContext.request.contextPath}/train" method="get">
    <input type="submit" name="getall" value="GetAll" />
</form>

<form action="${pageContext.request.contextPath}/addtrain.jsp">
    <input type="submit" name="saveone" value="SaveOne" />
</form>

<div>
    <form action="${pageContext.request.contextPath}/train/update" method="post">
        <fieldset>
            <legend>Update passenger</legend>
            ID : <input type="number" name="id" value="${object.getId()}"/>
        </fieldset>
        <input type="submit" name="upd" value="Update"/>
    </form>
</div>

<div>
    <form action="${pageContext.request.contextPath}/train/delete" method="get">
        <fieldset>
            <legend>Delete passenger</legend>
            ID : <input type="number" name="id" value="${object.getId()}"/>
        </fieldset>
        <input type="submit" name="del" value="Delete"/>
    </form>

</div>





</body>
</html>
