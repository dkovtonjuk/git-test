
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add passenger</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add passenger</h2>
    <p>It is so lonely. Add them!</p>
    <form action="${pageContext.request.contextPath}/passenger/update/save" method="post">
        <fieldset>
            <legend>${dataString}_${object.getId()}</legend>
            <input type="hidden" name="id" value="${object.getId()}"/><br/><br/>
            Name : <input type="text" name="name" value="${object.getName()}"/><br/><br/>
            Email : <input type="text" name="email" value="${object.getEmail()}"/><br/><br/>
            PhoneNumber : <input type="number" name="phoneNumber" value="${object.getPhoneNumber()}"/><br/><br/>
            TrainNumber : <input type="number" name="trainNumber" value="${object.getTrainNumber()}"/><br/><br/>
            ProdYear : <input type="number" name="seatNumber" value="${object.getSeatNumber()}"/><br/><br/>
        </fieldset>
        <input type="submit" name="add" value="Save" />
    </form>


</div>

</body>
</html>