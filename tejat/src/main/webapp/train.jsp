
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>train</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Trains</h2>
    <%--<form action="/navigation.jsp" method="post">--%>
    <form action="/navigation.jsp">
        <button type="submit">Go to the navigation page</button><br>
        <br>
    </form>
    <p>The table shows all railcars:</p>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Number</th>
            <th>Model</th>
            <th>DepartureStation</th>
            <th>ArrivalStation</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="trainList" items="${atr}">
            <tr>
                <td><c:out value="${trainList.id}" /></td>
                <td><c:out value="${trainList.number}" /></td>
                <td><c:out value="${trainList.model}" /></td>
                <td><c:out value="${trainList.departureStation}" /></td>
                <td><c:out value="${trainList.arrivalStation}" /></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
