
<!DOCTYPE html>
<html lang="en">
<head>
    <title>upd train</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update train</h2>
    <p>Do it!</p>
    <form action="${pageContext.request.contextPath}/train/update/save" method="post">
        <fieldset>
            <legend>${dataString}_${object.getId()}</legend>
            <input type="hidden" name="id" value="${object.getId()}"/><br/><br/>
            Number : <input type="number" name="number" value="${object.getNumber()}"/><br/><br/>
            Model : <input type="text" name="model" value="${object.getModel()}"/><br/><br/>
            DepartureStation : <input type="text" name="departureStation" value="${object.getDepartureStation()}"/><br/><br/>
            ArrivalStation : <input type="text" name="arrivalStation" value="${object.getArrivalStation()}"/><br/><br/>
        </fieldset>
        <input type="submit" name="add" value="Save" />
    </form>

</div>

</body>
</html>