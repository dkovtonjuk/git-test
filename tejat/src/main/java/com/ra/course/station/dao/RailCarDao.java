package com.ra.course.station.dao;

import com.ra.course.station.configuration.DaoUtils;
import com.ra.course.station.entity.Railcar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;

@Component
public class RailCarDao implements GenericDao<Railcar> {

    final transient JdbcTemplate jdbcTemplate;

    @Autowired
    public RailCarDao(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Railcar> saveAll(final List<Railcar> railCars) {
        railCars.forEach(this::save);
        return railCars;
    }

    @Override
    public Railcar findOne(final long id) {
        return jdbcTemplate.queryForObject(FIND_ONE_RAILCAR, BeanPropertyRowMapper.newInstance(Railcar.class), id);
    }

    @Override
    public Railcar save(final Railcar railcar) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection con) throws SQLException {
                final PreparedStatement pstm = con.prepareStatement(INSERT_RAILCAR);
                pstm.setString(1, railcar.getType());
                pstm.setInt(2, railcar.getCapasity());
                pstm.setInt(3, railcar.getProductionYear());
                return pstm;
            }
        }, keyHolder);
        railcar.setId(DaoUtils.setId(keyHolder));
        return railcar;
    }

    @Override
    public List<Railcar> findAll() {
        return this.jdbcTemplate.query(FIND_ALL_RAILCARS, BeanPropertyRowMapper.newInstance(Railcar.class));
    }

    @Override
    public int update(final Railcar railcar) {
        return this.jdbcTemplate.update(UPDATE_RAILCAR, railcar.getType(), railcar.getCapasity(), railcar.getProductionYear(), railcar.getId());
    }

    @Override
    public int delete(final long id) {
        return jdbcTemplate.update(DELETE_RAILCAR, id);
    }

}
