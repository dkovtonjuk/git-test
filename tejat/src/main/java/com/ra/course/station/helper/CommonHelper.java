package com.ra.course.station.helper;

public final class CommonHelper {

    private CommonHelper() {
    }

    public static Long getId(final String idStr) {
        if (idStr != null && !idStr.isEmpty()) {
            return Long.valueOf(idStr);
        }
        return 0L;
    }

    public static Integer getIntId(final String idStr) {
        if (idStr != null && !idStr.isEmpty()) {
            return Integer.valueOf(idStr);
        }
        return 0;
    }

}
