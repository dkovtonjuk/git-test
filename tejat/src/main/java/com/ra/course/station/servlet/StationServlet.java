package com.ra.course.station.servlet;

import com.ra.course.station.command.RequestCommand;
import com.ra.course.station.configuration.JdbcConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/"})
public class StationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private transient AnnotationConfigApplicationContext context;

    @Override
    public void init() {
        if (context == null) {
            context = new AnnotationConfigApplicationContext(JdbcConfig.class);
        }
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        if(context.containsBean(req.getPathInfo())){
context.getBean(req.getPathInfo(), RequestCommand.class).get(req, resp);
        }
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        if(context.containsBean(req.getPathInfo())){
            context.getBean(req.getPathInfo(), RequestCommand.class).get(req, resp);
        }
    }
}
