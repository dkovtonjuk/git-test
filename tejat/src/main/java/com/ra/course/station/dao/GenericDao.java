package com.ra.course.station.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T extends Serializable  > {

    List <T>  saveAll(final List<T> obj);

    T save(final T obj);

    T findOne(final long id);

    List<T> findAll();

    int update(final T obj);

    int delete(final long id);
}
