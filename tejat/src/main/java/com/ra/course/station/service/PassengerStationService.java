package com.ra.course.station.service;

import com.ra.course.station.dao.PassengerDao;
import com.ra.course.station.dto.PassengerDto;
import com.ra.course.station.entity.Passenger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassengerStationService implements StationService<Passenger, PassengerDto>{

    private final PassengerDao passengerDao;

    @Autowired
    public PassengerStationService(PassengerDao passengerDao) {
        this.passengerDao = passengerDao;
    }


    @Override
    public Passenger save(PassengerDto element) {
        return null;
    }
}
