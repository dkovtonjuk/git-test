package com.ra.course.station.entity;

import java.io.Serializable;

public class Railcar implements Serializable {

    static final long serialVersionUID = 1L;
    private long id;
    private String type;
    private Integer capasity;
    private Integer ProductionYear;


    public Railcar(long id, String type, Integer capasity, Integer productionYear) {
        this.id = id;
        this.type = type;
        this.capasity = capasity;
        ProductionYear = productionYear;
    }

    public Railcar() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCapasity() {
        return capasity;
    }

    public void setCapasity(Integer capasity) {
        this.capasity = capasity;
    }

    public Integer getProductionYear() {
        return ProductionYear;
    }

    public void setProductionYear(Integer productionYear) {
        ProductionYear = productionYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Railcar)) return false;

        Railcar railcar = (Railcar) o;

        if (getId() != railcar.getId()) return false;
        if (getType() != null ? !getType().equals(railcar.getType()) : railcar.getType() != null) return false;
        if (getCapasity() != null ? !getCapasity().equals(railcar.getCapasity()) : railcar.getCapasity() != null)
            return false;
        return getProductionYear() != null ? getProductionYear().equals(railcar.getProductionYear()) : railcar.getProductionYear() == null;
    }

    private transient int result;

    @Override
    public int hashCode() {
        result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getCapasity() != null ? getCapasity().hashCode() : 0);
        result = 31 * result + (getProductionYear() != null ? getProductionYear().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Railcar{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", capasity=" + capasity +
                ", ProductionYear=" + ProductionYear +
                '}';
    }
}
