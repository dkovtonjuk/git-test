package com.ra.course.station.configuration;

public class SQLStatementsUtils {
    public static final String INSERT_PASSENGER =
            "INSERT INTO passenger(name,email,phoneNumber,trainNumber,seatNumber) VALUES ( ?,?,?,?,?);";

    public static final String DELETE_PASSENGER =
            "DELETE FROM passenger WHERE id = ?;";

    public static final String UPDATE_PASSENGER =
            "UPDATE passenger SET name = ?,email = ?,phoneNumber=?,trainNumber=?,seatNumber=? WHERE id=?;";

    public static final String FIND_PASSENGER = "SELECT *  FROM passenger WHERE passenger.id = ?;";

    public static final String FINDALL_PSNGRS = "SELECT * FROM passenger";

    public static final String CR_PASSENGER = "CREATE TABLE IF NOT EXISTS passenger(\n" +
            " id INT AUTO_INCREMENT PRIMARY KEY,\n" +
            " name VARCHAR (255) NOT NULL,\n" +
            " email VARCHAR (255) NOT NULL,\n" +
            " phoneNumber INTEGER(16),\n" +
            " trainNumber INTEGER(16),\n" +
            " seatNumber INTEGER(16) );";


    public static final String INSERT_RAILCAR =
            "INSERT INTO RAILCARS1 (TYPE,CAPASITY,PRODUCTIONYEAR) VALUES ( ?,?,?);";
    public static final String DELETE_RAILCAR =
            "DELETE FROM RAILCARS1 WHERE ID = ?;";

    public static final String UPDATE_RAILCAR =
            "UPDATE RAILCARS1 SET TYPE = ?,CAPASITY = ?,PRODUCTIONYEAR=? WHERE ID=?;";

    public static final String FIND_ONE_RAILCAR = "SELECT * FROM RAILCARS1 WHERE RAILCARS1.ID = ?;";

    public static final String FIND_ALL_RAILCARS = "SELECT * FROM RAILCARS1";

    public static final String CR_RAILCARS1 = "CREATE TABLE IF NOT EXISTS RAILCARS1(\n" +
            " ID  INT AUTO_INCREMENT PRIMARY KEY,\n" +
            " TYPE VARCHAR(255) NOT NULL,\n" +
            " CAPASITY INTEGER(16) NOT NULL,\n" +
            " PRODUCTIONYEAR INTEGER(16) NOT NULL);";


    public static final String CREATE_TRAIN = "INSERT INTO TRAIN (number,model,departure_station,arrival_station ) VALUES ( ?,?,?,?);";

    public static final String UPDATE_TRAIN =
            "UPDATE TRAIN SET number = ?, model = ?, departure_station = ?, arrival_station = ? WHERE ID = ?;";

    public static final String DELETE_TRAIN = "DELETE FROM TRAIN WHERE id = ?";

    public static final String READ_TRAIN = "SELECT * FROM TRAIN WHERE TRAIN.ID = ?";

    public static final String READ_ALL_TRAINS = "SELECT * FROM TRAIN";

    public static final String CR_TRAIN = "create table if not exists train(\n" +
            " id INT AUTO_INCREMENT PRIMARY KEY,\n" +
            " number varchar(255)not null,\n" +
            " model varchar(255)not null,\n" +
            " departure_station varchar(255),\n" +
            " arrival_station varchar(255) );";
}
