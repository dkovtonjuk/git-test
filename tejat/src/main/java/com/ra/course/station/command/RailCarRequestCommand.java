package com.ra.course.station.command;

import com.ra.course.station.dao.RailCarDao;
import com.ra.course.station.entity.Railcar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.ra.course.station.helper.CommonHelper.getId;
import static com.ra.course.station.helper.CommonHelper.getIntId;

@Service("railc")
public class RailCarRequestCommand implements RequestCommand {

    public transient RailCarDao railCarDao;

    @Autowired
    public RailCarRequestCommand(RailCarDao railCarDao) {
        this.railCarDao = railCarDao;
    }

    @Override
    public void post(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String pathInfo = request.getPathInfo();
        if (pathInfo.equals("/insert/one")) {
            saveOne(request, response);
        } else if (pathInfo.equals("/update")) {
            final Long id = getId(request.getParameter("id"));
            request.setAttribute("object", railCarDao.findOne(id));
            forward(request, response);
        } else if (pathInfo.equals("/update/save")) {
            doPut(request, response);
        } else {
            final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    public void get(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String pathInfo = req.getPathInfo();
        if (pathInfo != null && !pathInfo.isEmpty() && pathInfo.equals("/delete")) {
            delete(req, resp);
        }
        showAll(req, resp);
    }


    private void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        railCarDao.update(getRailCarFromRequest(request));
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(request, response);
    }

    private void delete(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final Long id = getId(request.getParameter("id"));
        railCarDao.delete(id);
    }


    private void showAll(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("atr", railCarDao.findAll());
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/railcar.jsp");
        dispatcher.forward(req, resp);
    }

    private void saveOne(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        railCarDao.save(getRailCarFromRequest(req));
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(req, resp);
    }

    private void forward(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/updaterailcar.jsp");
        dispatcher.forward(request, response);
        showAll(request, response);
    }

    private static Railcar getRailCarFromRequest(final HttpServletRequest request) {
        final Long id = getId(request.getParameter("id"));
        final String type = request.getParameter("type");
        final int capacity = getIntId(request.getParameter("capacity"));
        final int prodyear = getIntId(request.getParameter("prodyear"));
        return new Railcar(id, type, capacity, prodyear);
    }
}
