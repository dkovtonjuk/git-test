package com.ra.course.station.exception;

public class DataException extends RuntimeException {
    static final long serialVersionUID = 1L;

    public DataException(final String message) {
        super(message);
    }

    public DataException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
