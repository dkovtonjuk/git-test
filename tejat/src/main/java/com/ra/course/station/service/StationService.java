package com.ra.course.station.service;

import com.ra.course.station.dto.GenericDto;

public interface StationService<T, E extends GenericDto>{
T save (E element);
}
