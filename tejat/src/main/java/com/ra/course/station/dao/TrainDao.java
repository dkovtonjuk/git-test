package com.ra.course.station.dao;

import com.ra.course.station.configuration.DaoUtils;
import com.ra.course.station.entity.Train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;

@Component
public class TrainDao implements GenericDao<Train> {

    final transient JdbcTemplate jdbcTemplate;

    @Autowired
    public TrainDao(final JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Train> saveAll(final List<Train> trains) {
        trains.forEach(this::save);
        return trains;
    }

    @Override
    public Train findOne(final long id) {
        return jdbcTemplate.queryForObject(READ_TRAIN, BeanPropertyRowMapper.newInstance(Train.class), id);
    }

    @Override
    public Train save(final Train train) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection con) throws SQLException {
                final PreparedStatement pstm = con.prepareStatement(CREATE_TRAIN);
                pstm.setObject(1, train.getNumber());
                pstm.setObject(2, train.getModel());
                pstm.setObject(3, train.getDepartureStation());
                pstm.setObject(4, train.getArrivalStation());
                return pstm;
            }
        }, keyHolder);
        train.setId(DaoUtils.setId(keyHolder));
        return train;
    }

    @Override
    public List<Train> findAll() {
        return this.jdbcTemplate.query(READ_ALL_TRAINS, BeanPropertyRowMapper.newInstance(Train.class));
    }

    @Override
    public int update(final Train train) {
        return this.jdbcTemplate.update(UPDATE_TRAIN, train.getNumber(), train.getModel(), train.getDepartureStation(), train.getArrivalStation(), train.getId());
    }

    @Override
    public int delete ( final long id){
        return jdbcTemplate.update(DELETE_TRAIN, id);
    }

}
