package com.ra.course.station.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface RequestCommand {

    void post(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    void get(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
