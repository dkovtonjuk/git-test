package com.ra.course.station.dao;

import com.ra.course.station.configuration.DaoUtils;
import com.ra.course.station.entity.Passenger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;

@Component
public class PassengerDao implements GenericDao<Passenger> {

    final transient JdbcTemplate jdbcTemplate;

    @Autowired
    public PassengerDao(final JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Passenger> saveAll(final List<Passenger> passengers) {
        passengers.forEach(this::save);
        return passengers;
    }

    @Override
    public Passenger findOne(final long id) {
        return jdbcTemplate.queryForObject(FIND_PASSENGER, BeanPropertyRowMapper.newInstance(Passenger.class), id);
    }

    @Override
    public Passenger save(final Passenger passenger) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(final Connection con) throws SQLException {
                final PreparedStatement pstm = con.prepareStatement(INSERT_PASSENGER);
                pstm.setString(1, passenger.getName());
                pstm.setString(2, passenger.getEmail());
                pstm.setInt(3, passenger.getPhoneNumber());
                pstm.setInt(4, passenger.getTrainNumber());
                pstm.setInt(5, passenger.getSeatNumber());
                return pstm;
            }
        }, keyHolder);
        passenger.setId(DaoUtils.setId(keyHolder));
        return passenger;
    }

    @Override
    public List<Passenger> findAll() {
        return this.jdbcTemplate.query(FINDALL_PSNGRS, BeanPropertyRowMapper.newInstance(Passenger.class));
    }

    @Override
    public int update(final Passenger passenger) {
        return this.jdbcTemplate.update(UPDATE_PASSENGER, passenger.getName(), passenger.getEmail(), passenger.getPhoneNumber(), passenger.getTrainNumber(), passenger.getSeatNumber(), passenger.getId());
    }

    @Override
    public int delete ( final long id){
        return jdbcTemplate.update(DELETE_PASSENGER, id);
    }

}