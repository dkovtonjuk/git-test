package com.ra.course.station.command;

import com.ra.course.station.dao.TrainDao;
import com.ra.course.station.entity.Train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.ra.course.station.helper.CommonHelper.getId;

@Service("trn")
public class TrainRequestCommand implements RequestCommand {

    public transient TrainDao trainDao;

    @Autowired
    public TrainRequestCommand(TrainDao trainDao) {
        this.trainDao = trainDao;

    }

    @Override
    public void post(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String pathInfo = request.getPathInfo();
        if (pathInfo.equals("/insert/one")) {
            saveOne(request, response);
        } else if (pathInfo.equals("/update")) {
            final Long id = getId(request.getParameter("id"));
            request.setAttribute("object", trainDao.findOne(id));
            forward(request, response);
        } else if (pathInfo.equals("/update/save")) {
            doPut(request, response);
        } else {
            final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    public void get(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String pathInfo = req.getPathInfo();
        if (pathInfo != null && !pathInfo.isEmpty() && pathInfo.equals("/delete")) {
            delete(req, resp);
        }
        showAll(req, resp);
    }


    private void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        trainDao.update(getTrainFromRequest(request));
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(request, response);
    }


    private void delete(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final Long id = getId(request.getParameter("id"));
        trainDao.delete(id);
    }


    private void showAll(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("atr", trainDao.findAll());
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/train.jsp");
        dispatcher.forward(req, resp);
    }

    private void saveOne(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        trainDao.save(getTrainFromRequest(req));
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(req, resp);
    }

    private void forward(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/updatetrain.jsp");
        dispatcher.forward(request, response);
        showAll(request, response);
    }

    private static Train getTrainFromRequest(final HttpServletRequest request) {
        final Long id = getId(request.getParameter("id"));
        final String number = request.getParameter("number");
        final String model = request.getParameter("model");
        final String departureStation = request.getParameter("departureStation");
        final String arrivalStation = request.getParameter("arrivalStation");
        final Train train =  new Train(id, number, model, departureStation, arrivalStation);
        return train;
    }
}
