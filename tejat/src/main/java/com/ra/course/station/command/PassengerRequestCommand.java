package com.ra.course.station.command;

import com.ra.course.station.dto.PassengerDto;
import org.springframework.stereotype.Service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.ra.course.station.helper.CommonHelper.getId;
import static com.ra.course.station.helper.CommonHelper.getIntId;

@Service("pasngr")
public class PassengerRequestCommand implements RequestCommand{




    @Override
    public void post(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String pathInfo = request.getPathInfo();
        if (pathInfo.equals("/insert/one")) {
            saveOne(request, response);
        } else if (pathInfo.equals("/update")) {
            final Long id = getId(request.getParameter("id"));
            request.setAttribute("object", passengerDao.findOne(id));
            forward(request, response);
        } else if (pathInfo.equals("/update/save")) {
            doPut(request, response);
        } else {
            final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
            dispatcher.forward(request, response);
        }
    }

    @Override
    public void get(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String pathInfo = req.getPathInfo();
        if (pathInfo != null && !pathInfo.isEmpty() && pathInfo.equals("/delete")) {
            delete(req, resp);
        }
        showAll(req, resp);
    }


    private void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        passengerDao.update(getPassengerFromRequest(request));
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(request, response);
    }


    private void delete(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final Long id = getId(request.getParameter("id"));
        passengerDao.delete(id);
    }


    private void showAll(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("atr", passengerDao.findAll());
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/passenger.jsp");
        dispatcher.forward(req, resp);
    }

    private void saveOne(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        passengerDao.save(getPassengerFromRequest(req));
        final RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/navigation.jsp");
        dispatcher.forward(req, resp);
    }

    public void forward(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/updatepassenger.jsp");
        dispatcher.forward(request, response);
        showAll(request, response);
    }

    public static PassengerDto getPassengerFromRequest(final HttpServletRequest request) {
        final Long id = getId(request.getParameter("id"));
        final String name = request.getParameter("name");
        final String email = request.getParameter("email");
        final Integer phoneNumber = getIntId(request.getParameter("phoneNumber"));
        final Integer trainNumber = getIntId(request.getParameter("trainNumber"));
        final Integer seatNumber = getIntId(request.getParameter("seatNumber"));
        return new PassengerDto(id, name, email, phoneNumber, trainNumber, seatNumber);
    }
}
