package com.ra.course.station.dto;

import java.io.Serializable;

public class PassengerDto extends GenericDto implements Serializable {
    static final long serialVersionUID = 1L;
    private long id;
    private String name;
    private String email;
    private Integer phoneNumber;
    private Integer trainNumber;
    private Integer seatNumber;

    public PassengerDto(long id, String name, String email, Integer phoneNumber, Integer trainNumber, Integer seatNumber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.trainNumber = trainNumber;
        this.seatNumber = seatNumber;
    }


    public PassengerDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(Integer trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassengerDto)) return false;

        PassengerDto that = (PassengerDto) o;

        if (getId() != that.getId()) return false;
        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        if (getEmail() != null ? !getEmail().equals(that.getEmail()) : that.getEmail() != null) return false;
        if (getPhoneNumber() != null ? !getPhoneNumber().equals(that.getPhoneNumber()) : that.getPhoneNumber() != null)
            return false;
        if (getTrainNumber() != null ? !getTrainNumber().equals(that.getTrainNumber()) : that.getTrainNumber() != null)
            return false;
        return getSeatNumber() != null ? getSeatNumber().equals(that.getSeatNumber()) : that.getSeatNumber() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getPhoneNumber() != null ? getPhoneNumber().hashCode() : 0);
        result = 31 * result + (getTrainNumber() != null ? getTrainNumber().hashCode() : 0);
        result = 31 * result + (getSeatNumber() != null ? getSeatNumber().hashCode() : 0);
        return result;
    }
}
