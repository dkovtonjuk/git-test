package com.ra.course.station.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@EnableWebMvc
@Configuration
@ComponentScan({"com.ra.course.station", "com.ra.course.station.configuration"})
@PropertySource("classpath:datasource.properties")
public class JdbcConfig {

    @Autowired
    transient Environment environment;

    @Autowired
    transient ResourceLoader resourceLoader;

    @Bean
    public JdbcTemplate jdbcTemplate(final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DataSource dataSource() {
        final HikariConfig cfg = new HikariConfig();
        cfg.setJdbcUrl(environment.getProperty("db.url"));
        cfg.setUsername(environment.getProperty("db.username"));
        cfg.setPassword(environment.getProperty("db.password"));
        cfg.setDriverClassName(environment.getProperty("db.driver"));
        final DataSource dataSource = new HikariDataSource(cfg);
        return dataSource;
    }

//    @Bean
//    public ViewResolver viewResolver() {
//        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//        viewResolver.setViewClass(JstlView.class);
//        viewResolver.setPrefix("/WEB-INF/views/");
//        viewResolver.setSuffix(".jsp");
//        return viewResolver;
//    }

}
