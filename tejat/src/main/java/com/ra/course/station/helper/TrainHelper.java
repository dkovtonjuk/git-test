package com.ra.course.station.helper;

import com.ra.course.station.entity.Train;

import javax.servlet.http.HttpServletRequest;

import static com.ra.course.station.helper.CommonHelper.getId;

public final class TrainHelper {

    private TrainHelper() {
    }

    public static Train generateObject() {
       final Train train = new Train(22, "23", "443", "kiev", "minsk");
        return train;
    }

    public static Train getTrainFromRequest(final HttpServletRequest request) {
        final Long id = getId(request.getParameter("id"));
        final String number = request.getParameter("number");
        final String model = request.getParameter("model");
        final String departureStation = request.getParameter("departureStation");
        final String arrivalStation = request.getParameter("arrivalStation");
        final Train train =  new Train(id, number, model, departureStation, arrivalStation);
        return train;
    }

}
