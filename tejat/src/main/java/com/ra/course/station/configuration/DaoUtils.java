package com.ra.course.station.configuration;

import com.ra.course.station.entity.Passenger;
import com.ra.course.station.entity.Railcar;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class DaoUtils {

    private DaoUtils() {
    }

    public static PreparedStatement fillStatementWithRailcarData(
            final PreparedStatement insertStatement, final Railcar railcar) throws SQLException {
        insertStatement.setString(1, railcar.getType());
        insertStatement.setInt(2, railcar.getCapasity());
        insertStatement.setInt(3, railcar.getProductionYear());
        return insertStatement;
    }

    public static Railcar parseRailcarRow(final ResultSet rs) throws SQLException {
        final Railcar railcar = new Railcar(1, "1", 1, 1);
        railcar.setId(rs.getLong(1));
        railcar.setType(rs.getString(2));
        railcar.setCapasity(rs.getInt(3));
        railcar.setProductionYear(rs.getInt(4));
        return railcar;
    }

    public static PreparedStatement fillStatementWithPassengerData(
            final PreparedStatement insertStatement, final Passenger passenger) throws SQLException {
        insertStatement.setString(1, passenger.getName());
        insertStatement.setString(2, passenger.getEmail());
        insertStatement.setInt(3, passenger.getPhoneNumber());
        insertStatement.setInt(4, passenger.getTrainNumber());
        insertStatement.setInt(5, passenger.getSeatNumber());
        return insertStatement;
    }

    public static Passenger parsePassengerRow(final ResultSet rs) throws SQLException {
        final Passenger passenger = new Passenger();
        passenger.setId(rs.getLong(1));
        passenger.setName(rs.getString(2));
        passenger.setEmail(rs.getString(3));
        passenger.setPhoneNumber(rs.getInt(4));
        passenger.setTrainNumber(rs.getInt(5));
        passenger.setSeatNumber(rs.getInt(6));
        return passenger;
    }

    public static Long setId(final KeyHolder keyHolder) {
        Long id = Long.valueOf(keyHolder.getKeyList().size());
        if (id > 0) {
            id = keyHolder.getKey().longValue();
        }
        return id;
    }

}
