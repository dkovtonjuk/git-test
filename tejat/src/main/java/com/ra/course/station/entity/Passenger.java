package com.ra.course.station.entity;

import java.io.Serializable;
import java.util.Objects;


public class Passenger implements Serializable {
    static final long serialVersionUID = 1L;
    private long id;
    private String name;
    private String email;
    private Integer phoneNumber;
    private Integer trainNumber;
    private Integer seatNumber;


    public Passenger() {
    }

    public Passenger(final long id, final String name, final String email, final Integer phoneNumber, final Integer trainNumber, final Integer seatNumber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.trainNumber = trainNumber;
        this.seatNumber = seatNumber;

    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(final Integer trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(final Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passenger passenger = (Passenger) o;
        return id == passenger.id &&
                Objects.equals(name, passenger.name) &&
                Objects.equals(email, passenger.email) &&
                Objects.equals(phoneNumber, passenger.phoneNumber) &&
                Objects.equals(trainNumber, passenger.trainNumber) &&
                Objects.equals(seatNumber, passenger.seatNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, phoneNumber, trainNumber, seatNumber);
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", trainNumber=" + trainNumber +
                ", seatNumber=" + seatNumber +

                '}';
    }
}
