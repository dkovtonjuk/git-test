package com.ra.course.station.exception;

/**
 * Base exception for RailwayStation application.
 */
public class TrainException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public TrainException(final String message) {
        super(message);
    }
}
