package com.ra.course.station.entity;

import java.io.Serializable;
import java.util.Objects;

public class Train implements Serializable {

    static final long serialVersionUID = 1L;

    private long id;
    private String number;
    private String model;
    private String departureStation;
    private String arrivalStation;

    public Train() {
    }

    public Train(long id, String number, String model, String departureStation, String arrivalStation) {
        this.id = id;
        this.number = number;
        this.model = model;
        this.departureStation = departureStation;
        this.arrivalStation = arrivalStation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return id == train.id &&
                Objects.equals(number, train.number) &&
                Objects.equals(model, train.model) &&
                Objects.equals(departureStation, train.departureStation) &&
                Objects.equals(arrivalStation, train.arrivalStation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, model, departureStation, arrivalStation);
    }

    @Override
    public String toString() {
        return "Train{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", departureStation='" + departureStation + '\'' +
                ", arrivalStation='" + arrivalStation + '\'' +
                '}';
    }
}
