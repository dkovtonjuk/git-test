package com.ra.course.station.servlet;

import com.ra.course.station.configuration.JdbcConfig;
import com.ra.course.station.dao.PassengerDao;
import com.ra.course.station.entity.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;
import static com.ra.course.station.helper.CommonHelper.getId;
import static com.ra.course.station.helper.PassengerHelper.generateObject;
import static com.ra.course.station.helper.PassengerHelper.getPassengerFromRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class PassengerServletMockTest {
    private PassengerServlet servlet = new PassengerServlet();
    private AnnotationConfigApplicationContext context = mock(AnnotationConfigApplicationContext.class);
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);
    private JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
    private JdbcConfig mockJdbcConfig = mock(JdbcConfig.class);

    @BeforeEach
    void init() {
        ReflectionTestUtils.setField(servlet, "context", context);
        servlet.passengerDao = new PassengerDao(mockJdbcTemplate);
    }

    @Test
    public void testInitBean() {
        assertNotNull(context);
        servlet.init();
        assertEquals(servlet.context.getBean("passengerDao"), null);
        assertEquals(servlet.context.getBean("jdbcTemplate"), null);
    }

    @Test
    public void testWhenCallDoGet_ThenReturnAttributes() throws ServletException, IOException {
        final String forwardedUrl = "/passenger.jsp";
        request.setPathInfo("/navigation");
        servlet.doGet(request, response);
        assertEquals(forwardedUrl, response.getForwardedUrl());
    }

    @Test
    public void testWhenCallUpdateObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/update");
        request.setAttribute("object", "object");
        request.setParameter("id", "1");
        servlet.doPost(request, response);
        final Long id = getId(request.getParameter("id"));
        Passenger object = servlet.passengerDao.findOne(id);
        servlet.forward(request, response);
        assertEquals(object, request.getAttribute("object"));
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenShowObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/passenger");
        request.addParameter("id", "1");
        servlet.doGet(request, response);
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenSaveObjectAfterUpdate_ThenReurnOne() throws ServletException, IOException {
        request.setPathInfo("/update/save");
        Passenger object = generateObject();
        servlet.doPost(request, response);
        when(mockJdbcTemplate.update(UPDATE_PASSENGER, object.getName(), object.getEmail(), object.getPhoneNumber(), object.getTrainNumber(), object.getSeatNumber(), object.getId())).thenReturn(1);
        assertEquals(1, servlet.passengerDao.update(object));
    }

    @Test
    public void testWhenCallDoPostWithoutPath() throws ServletException, IOException {
        request.setPathInfo("/navigation.jsp");
        servlet.doPost(request, response);
        assertNotNull(request.getPathInfo());
    }

    @Test
    public void testWhenInsertObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/insert/one");
        servlet.doPost(request, response);
        assertNotNull(getPassengerFromRequest(request));
    }

    @Test
    public void testWhenSaveObject_ThenReturnAttributes() throws ServletException, IOException, SQLException {
        request.setPathInfo("/insert/one");
        Connection mockConnection = mock(Connection.class);
        PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
        Passenger mockObj = mock(Passenger.class);
        servlet.doPost(request, response);
        when(mockConnection.prepareStatement(
                eq(INSERT_PASSENGER))).thenReturn(mockPreparedStatement);
        doAnswer(invocation -> {
                    ((PreparedStatementCreator) invocation.getArguments()[0]).createPreparedStatement(mockConnection);
                    verify(mockPreparedStatement, times(2)).setString(any(Integer.class), any(String.class));
                    verify(mockPreparedStatement, times(3)).setInt(any(Integer.class), any(Integer.class));
                    return null;
                }
        ).when(mockJdbcTemplate).update(any(PreparedStatementCreator.class), any(KeyHolder.class));
        assertEquals(servlet.passengerDao.save(mockObj), mockObj);
    }

    @Test
    void testWhenCallUpdateObject_ThenUpdateIt() throws ServletException, IOException {
        request.setPathInfo("/update");
        request.addParameter("id", "1");
        servlet.doGet(request, response);
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenCallDeleteObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/delete");
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(1);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(1);
        assertEquals(1, mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id")));
    }

    @Test
    public void whenDeleteOneEmptyPath_ThenReturnNull() throws ServletException, IOException {
        request.setPathInfo("");
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(0);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(0);
        assertEquals(0, mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id")));
    }

    @Test
    public void whenDeleteOneNullPath_ThenReturnNull() throws ServletException, IOException {
        request.setPathInfo(null);
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(0);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id"))).thenReturn(0);
        assertEquals(0, mockJdbcTemplate.update(DELETE_PASSENGER, request.getParameter("id")));
    }

}
