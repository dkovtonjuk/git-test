package com.ra.course.station.servlet;

import com.ra.course.station.dao.RailCarDao;
import com.ra.course.station.entity.Railcar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;
import static com.ra.course.station.helper.CommonHelper.getId;
import static com.ra.course.station.helper.RailCarHelper.generateObject;
import static com.ra.course.station.helper.RailCarHelper.getRailCarFromRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class RailCarServletMockTest {
    private RailCarServlet servlet = new RailCarServlet();
    private AnnotationConfigApplicationContext context = mock(AnnotationConfigApplicationContext.class);
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();
    private JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);

    @BeforeEach
    public void init() {
        ReflectionTestUtils.setField(servlet, "context", context);
        servlet.railCarDao = new RailCarDao(mockJdbcTemplate);
    }

    @Test
    public void testInitBean() {
        servlet.init();
        assertEquals(servlet.context.getBean("railCarDao"), null);
        assertEquals(servlet.context.getBean("jdbcTemplate"), null);
    }

    @Test
    public void testWhenCallDoGet_ThenReturnAttributes() throws ServletException, IOException {
        final String forwardedUrl = "/railcar.jsp";
        request.setPathInfo("/navigation");
        servlet.doGet(request, response);
        assertEquals(forwardedUrl, response.getForwardedUrl());
    }

    @Test
    public void testWhenCallUpdateObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/update");
        request.setAttribute("object", "object");
        request.setParameter("id", "1");
        servlet.doPost(request, response);
        final Long id = getId(request.getParameter("id"));
        Railcar object = servlet.railCarDao.findOne(id);
        servlet.forward(request, response);
        assertEquals(object, request.getAttribute("object"));
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenShowObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/railcar");
        request.addParameter("id", "1");
        servlet.doGet(request, response);
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenSaveObjectAfterUpdate_ThenReurnOne() throws ServletException, IOException {
        request.setPathInfo("/update/save");
        Railcar object = generateObject();
        servlet.doPost(request, response);
        when(mockJdbcTemplate.update(UPDATE_RAILCAR, object.getType(), object.getCapasity(), object.getProductionYear(), object.getId())).thenReturn(1);
        assertEquals(1, servlet.railCarDao.update(object));
    }

    @Test
    public void testWhenInsertObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/insert/one");
        servlet.doPost(request, response);
        assertNotNull(getRailCarFromRequest(request));
    }

    @Test
    public void testWhenSaveObject_ThenReturnAttributes() throws ServletException, IOException, SQLException {
        request.setPathInfo("/insert/one");
        Connection mockConnection = mock(Connection.class);
        PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
        Railcar mockObj = mock(Railcar.class);
        servlet.doPost(request, response);
        when(mockConnection.prepareStatement(
                eq(INSERT_RAILCAR))).thenReturn(mockPreparedStatement);
        doAnswer(invocation -> {
                    ((PreparedStatementCreator) invocation.getArguments()[0]).createPreparedStatement(mockConnection);
                    verify(mockPreparedStatement, times(1)).setString(any(Integer.class), any(String.class));
                    verify(mockPreparedStatement, times(2)).setInt(any(Integer.class), any(Integer.class));
                    return null;
                }
        ).when(mockJdbcTemplate).update(any(PreparedStatementCreator.class), any(KeyHolder.class));
        assertEquals(servlet.railCarDao.save(mockObj), mockObj);
    }

    @Test
    void testWhenCallUpdateObject_ThenUpdateIt() throws ServletException, IOException {
        request.setPathInfo("/update");
        request.addParameter("id", "1");
        servlet.doGet(request, response);
        assertEquals("1", request.getParameter("id"));
    }

    @Test
    public void testWhenCallDeleteObject_ThenReturnAttributes() throws ServletException, IOException {
        request.setPathInfo("/delete");
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(1);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(1);
        assertEquals(1, mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id")));
    }

    @Test
    public void testWhenCallDeleteObjectEmptyPath_ThenReturnZero() throws ServletException, IOException {
        request.setPathInfo("");
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(0);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(0);
        assertEquals(0, mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id")));
    }

    @Test
    public void testWhenCallDoPostWithoutPath() throws ServletException, IOException {
        request.setPathInfo("/navigation.jsp");
        servlet.doPost(request, response);
        assertNotNull(request.getPathInfo());
    }

    @Test
    public void testWhenCallDeleteObjectNullPath_ThenReturnZero() throws ServletException, IOException {
        request.setPathInfo(null);
        final String id = "1";
        request.addParameter("id", id);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(0);
        servlet.doGet(request, response);
        when(mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id"))).thenReturn(0);
        assertEquals(0, mockJdbcTemplate.update(DELETE_RAILCAR, request.getParameter("id")));
    }

}
