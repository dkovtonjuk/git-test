package com.ra.course.station.helper;

import org.junit.jupiter.api.Test;

import static com.ra.course.station.helper.CommonHelper.getId;
import static com.ra.course.station.helper.CommonHelper.getIntId;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommonHelperMockTest {
    @Test
    public void testWhenStringIdIsNotNullAndNotEmptyThenReturnLongId() {
        String id = "2";
        assertEquals((Long) 2L, getId(id));
    }

    @Test
    public void testWhenStringIdIsNullThenReturnLongId() {
        String id = null;
        assertEquals((Long)0L, getId(id));
    }

    @Test
    public void testWhenStringIdIsEmptyThenReturnLongId() {
        String id = "";
        assertEquals((Long)0L, getId(id));
    }

    @Test
    public void testWhenStringIdIsNotNullAndNotEmptyThenReturnIntId() {
        String id = "2";
        assertEquals((Integer)2, getIntId(id));
    }

    @Test
    public void testWhenStringIdIsNullThenReturnIntId() {
        String id = null;
        assertEquals((Integer)0, getIntId(id));
    }

    @Test
    public void testWhenStringIdIsEmptyThenReturnIntId() {
        String id = "";
        assertEquals((Integer)0, getIntId(id));
    }

}
