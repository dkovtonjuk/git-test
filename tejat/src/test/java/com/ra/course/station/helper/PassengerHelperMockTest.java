package com.ra.course.station.helper;

import com.ra.course.station.entity.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static com.ra.course.station.helper.PassengerHelper.generateObject;
import static com.ra.course.station.helper.PassengerHelper.getPassengerFromRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PassengerHelperMockTest {
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private long id = 0L;
    private String name = null;
    private String email = null;
    private Integer phoneNumber = 0;
    private Integer trainNumber = 0;
    private Integer seatNumber = 0;

    @BeforeEach
    public void init() {
        request.setParameter("id", "0");
        request.setParameter("name", name);
        request.setParameter("email", email);
        request.setParameter("phoneNumber", "0");
        request.setParameter("trainNumber", "0");
        request.setParameter("seatNumber", "0");
    }

    @Test
    public void testWhenCallGenerateObjectThenReturnNewObject() {
        assertEquals(new Passenger(22, "petya", "petya@gmail.com", 675675566, 55, 12), generateObject());
    }

    @Test
    public void testGetObjectFromRequest_Return_Railcar() {
        name = "bob";
        request.setParameter("name", name);
        Passenger passenger =  new Passenger(id, name, email, phoneNumber, trainNumber,seatNumber);
        assertEquals(passenger, getPassengerFromRequest(request));
    }

}
