package com.ra.course.station.dao;

import com.ra.course.station.entity.Railcar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class RailCarDaoMockTest {
    private JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
    private Railcar mockRailCar = mock(Railcar.class);
    private RailCarDao railCarDao = new RailCarDao(mockJdbcTemplate);
    Iterator<Railcar> iterator = mock(Iterator.class);
    Connection mockConnection = mock(Connection.class);
    PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    List<Railcar> mockList;

    @BeforeEach
    void init() throws SQLException {
        mockList = mock(List.class);
        when(mockList.iterator()).thenReturn(iterator);
    }

    @Test
    void testWhenCallFindRailCarById_ThenReturnItFromDb() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class), anyLong())).thenReturn(new Railcar());
        assertNotNull(railCarDao.findOne(1L));
    }

    @Test
    void testWhenCallSaveRailCar_ThenReturnNewRailCar() throws SQLException {
        when(mockConnection.prepareStatement(
                eq(INSERT_RAILCAR))).thenReturn(mockPreparedStatement);
        doAnswer(invocation -> {
            ((PreparedStatementCreator) invocation.getArguments()[0]).createPreparedStatement(mockConnection);
            verify(mockPreparedStatement, times(1)).setString(any(Integer.class), any(String.class));
            verify(mockPreparedStatement, times(2)).setInt(any(Integer.class), any(Integer.class));
            return null;
        }).when(mockJdbcTemplate).update(any(PreparedStatementCreator.class), any(KeyHolder.class));
        assertNotNull(railCarDao.save(mockRailCar));
    }

    @Test
    void testWhenCallSaveRailcarsList_ThenReturnListOfRailCars() {
        when(iterator.hasNext()).thenReturn(true);
        when(iterator.next()).thenReturn(mockRailCar);
        assertEquals(mockList, railCarDao.saveAll(mockList));
    }

    @Test
    void testWhenCallSaveAllRaiCars_ThenReturnNull() {
        when(iterator.hasNext()).thenReturn(false);
        assertEquals(0, railCarDao.saveAll(mockList).size());
    }

    @Test
    void testWhenCallfindAllRailCars_ThenReturnList() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class))).thenReturn(mockList);
        assertNotNull(railCarDao.findAll());
    }

    @Test
    void TestWhenCallDeleteRailCar_ThenReturnOne() {
        when(mockJdbcTemplate.update(DELETE_RAILCAR, mockRailCar.getId())).thenReturn(1);
        assertEquals(1, railCarDao.delete(mockRailCar.getId()));
    }

    @Test
    void testWhenCallDeleteRailCar_ThenReturnZero() {
        when(mockJdbcTemplate.update(DELETE_RAILCAR, mockRailCar.getId())).thenReturn(0);
        assertEquals(0, railCarDao.delete(mockRailCar.getId()));
    }

    @Test
    void testWhenCallUpdateRailCar_ThenReturnZero() {
        when(mockJdbcTemplate.update(UPDATE_RAILCAR, mockRailCar.getType(),
                mockRailCar.getCapasity(), mockRailCar.getProductionYear(), mockRailCar.getId())).thenReturn(0);
        assertEquals(0, railCarDao.update(mockRailCar));
    }

    @Test
    void testWhenCallUpdateRailCar_ThenReturnOne() {
        when(mockJdbcTemplate.update(UPDATE_RAILCAR, mockRailCar.getType(),
                mockRailCar.getCapasity(), mockRailCar.getProductionYear(), mockRailCar.getId())).thenReturn(1);
        assertEquals(1, railCarDao.update(mockRailCar));
    }

}
