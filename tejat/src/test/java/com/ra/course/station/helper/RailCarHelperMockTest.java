package com.ra.course.station.helper;

import com.ra.course.station.entity.Railcar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static com.ra.course.station.helper.RailCarHelper.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RailCarHelperMockTest {
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private Long id = 0L;
    private String type = null;
    private Integer capasity = 0;
    private Integer productionYear = 0;

    @BeforeEach
    public void init() {
        request.setParameter("id", "0");
        request.setParameter("type", type);
        request.setParameter("capasity", "0");
        request.setParameter("productionYear", "0");
    }

    @Test
    public void testWhenCallGenerateObjectThenReturnNewObject() {
        assertEquals(new Railcar(22, "passenger", 66, 1987), generateObject());
    }

    @Test
    public void testGetObjectFromRequest_Return_Railcar() {
        type = null;
        request.setParameter("type", type);
        Railcar railcar =  new Railcar(id, type, capasity, productionYear);
        assertEquals(railcar, getRailCarFromRequest(request));
    }

}

