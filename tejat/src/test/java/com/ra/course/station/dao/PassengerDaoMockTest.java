package com.ra.course.station.dao;

import com.ra.course.station.entity.Passenger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class PassengerDaoMockTest {

    private JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
    private Passenger mockPassenger;
    private PassengerDao passengerDao = new PassengerDao(mockJdbcTemplate);
    List<Passenger> mockList = mock(List.class);
    Iterator<Passenger> iterator = mock(Iterator.class);
    Connection mockConnection = mock(Connection.class);
    PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);

    @BeforeEach
    void init() {
        mockPassenger = mock(Passenger.class);
        when(mockList.iterator()).thenReturn(iterator);
    }

    @Test
    void testWhenCallFindPassengerById_ThenReturnItFromDb() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class), anyLong())).thenReturn(new Passenger());
        assertNotNull(passengerDao.findOne(1L));
    }

    @Test
    void testWhenCallSavePassenger_ThenReturnNewPassenger() throws SQLException {
        when(mockConnection.prepareStatement(
                eq(INSERT_PASSENGER))).thenReturn(mockPreparedStatement);
        doAnswer(invocation -> {
            ((PreparedStatementCreator) invocation.getArguments()[0]).createPreparedStatement(mockConnection);
            verify(mockPreparedStatement, times(2)).setString(any(Integer.class), any(String.class));
            verify(mockPreparedStatement, times(3)).setInt(any(Integer.class), any(Integer.class));
            return null;
        }).when(mockJdbcTemplate).update(any(PreparedStatementCreator.class), any(KeyHolder.class));
        assertNotNull(passengerDao.save(mockPassenger));
    }

    @Test
    void testWhenCallSavePassengersList_ThenReturnListOfPassengers() {
        when(iterator.hasNext()).thenReturn(true);
        when(iterator.next()).thenReturn(mockPassenger);
        assertEquals(mockList, passengerDao.saveAll(mockList));
    }

    @Test
    void testWhenCallSaveAllPassengers_ThenReturnNull() {
        when(iterator.hasNext()).thenReturn(false);
        assertEquals(0, passengerDao.saveAll(mockList).size());
    }

    @Test
    void testWhenCallfindAllPassengers_ThenReturnList() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class))).thenReturn(mockList);
        assertNotNull(passengerDao.findAll());
    }

    @Test
    void testWhenCallDeletePassenger_ThenReturnOne() {
        when(mockJdbcTemplate.update(DELETE_PASSENGER, mockPassenger.getId())).thenReturn(1);
        assertEquals(1, passengerDao.delete(mockPassenger.getId()));
    }

    @Test
    void testWhenCallDeletePassenger_ThenReturnZero() {
        when(mockJdbcTemplate.update(DELETE_PASSENGER, mockPassenger.getId())).thenReturn(0);
        assertEquals(0, passengerDao.delete(mockPassenger.getId()));
    }

    @Test
    void testWhenCallUpdatePassenger_ThenReturnZero() {
        when(mockJdbcTemplate.update(UPDATE_PASSENGER, mockPassenger.getName(),
                mockPassenger.getEmail(), mockPassenger.getPhoneNumber(), mockPassenger.getTrainNumber(), mockPassenger.getSeatNumber(), mockPassenger.getId())).thenReturn(0);
        assertEquals(0, passengerDao.update(mockPassenger));
    }

    @Test
    void testWhenCallUpdatePassenger_ThenReturnOne() {
        when(mockJdbcTemplate.update(UPDATE_PASSENGER, mockPassenger.getName(),
                mockPassenger.getEmail(), mockPassenger.getPhoneNumber(), mockPassenger.getTrainNumber(), mockPassenger.getSeatNumber(), mockPassenger.getId())).thenReturn(1);
        assertEquals(1, passengerDao.update(mockPassenger));

    }

}