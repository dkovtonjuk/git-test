package com.ra.course.station.helper;

import com.ra.course.station.entity.Train;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static com.ra.course.station.helper.TrainHelper.generateObject;
import static com.ra.course.station.helper.TrainHelper.getTrainFromRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrainHelperMockTest {
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private Long id = 0L;
    private String number = null;
    private String model = null;
    private String departureStation = null;
    private String arrivalStation = null;


    @BeforeEach
    public void init() {
        request.setParameter("id", "0");
        request.setParameter("number", number);
        request.setParameter("model", model);
        request.setParameter("departureStation", departureStation);
        request.setParameter("arrivalStation", arrivalStation);
    }

    @Test
    public void testWhenCallGenerateObjectThenReturnNewObject() {
        assertEquals(new Train(22, "23", "443", "kiev", "minsk"), generateObject());
    }

    @Test
    public void testGetObjectFromRequest_Return_Railcar() {
        number = "456";
        request.setParameter("number", number);
        assertEquals(new Train(id, number, model, departureStation, arrivalStation), getTrainFromRequest(request));
    }
}
