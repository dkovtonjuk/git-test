package com.ra.course.station.dao;

import com.ra.course.station.entity.Train;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import static com.ra.course.station.configuration.SQLStatementsUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class TrainDaoMockTest {
    private JdbcTemplate mockJdbcTemplate = mock(JdbcTemplate.class);
    private Train mockTrain;
    private TrainDao trainDao = new TrainDao(mockJdbcTemplate);
    List<Train> mockList = mock(List.class);
    Iterator<Train> iterator = mock(Iterator.class);
    Connection mockConnection = mock(Connection.class);
    PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);

    @BeforeEach
    void init() {
        mockTrain = mock(Train.class);
        when(mockList.iterator()).thenReturn(iterator);
    }

    @Test
    void testWhenCallFindTrainById_ThenReturnItFromDb() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class), anyLong())).thenReturn(new Train());
        assertNotNull(trainDao.findOne(1L));
    }

    @Test
    void testWhenCallSaveRailCar_ThenReturnNewRailCar() throws SQLException {
        when(mockConnection.prepareStatement(
                eq(CREATE_TRAIN))).thenReturn(mockPreparedStatement);
        doAnswer(invocation -> {
            ((PreparedStatementCreator) invocation.getArguments()[0]).createPreparedStatement(mockConnection);
            verify(mockPreparedStatement, times(4)).setObject(any(Integer.class), any(Object.class));
            return null;
        }).when(mockJdbcTemplate).update(any(PreparedStatementCreator.class), any(KeyHolder.class));
        assertNotNull(trainDao.save(mockTrain));
    }

    @Test
    void testWhenCallSaveTrainsList_ThenReturnListOfTrains() {
        when(iterator.hasNext()).thenReturn(true);
        when(iterator.next()).thenReturn(mockTrain);
        assertEquals(mockList, trainDao.saveAll(mockList));
    }

    @Test
    void testWhenCallSaveAllTrains_ThenReturnNull() {
        when(iterator.hasNext()).thenReturn(false);
        assertEquals(0, trainDao.saveAll(mockList).size());
    }

    @Test
    void testWhenCallfindAllTrains_ThenReturnList() {
        when(mockJdbcTemplate.queryForObject(anyString(), any(BeanPropertyRowMapper.class))).thenReturn(mockList);
        assertNotNull(trainDao.findAll());
    }

    @Test
    void testWhenCallDeleteTrain_ThenReturnOne() {
        when(mockJdbcTemplate.update(DELETE_TRAIN, mockTrain.getId())).thenReturn(1);
        assertEquals(1, trainDao.delete(mockTrain.getId()));
    }

    @Test
    void testWhenCallDeleteTrain_ThenReturnZero() {
        when(mockJdbcTemplate.update(DELETE_TRAIN, mockTrain.getId())).thenReturn(0);
        assertEquals(0, trainDao.delete(mockTrain.getId()));
    }

    @Test
    void testWhenCallUpdateTrain_ThenReturnZero() {
        when(mockJdbcTemplate.update(UPDATE_TRAIN, mockTrain.getNumber(),
                mockTrain.getModel(), mockTrain.getDepartureStation(), mockTrain.getArrivalStation(), mockTrain.getId())).thenReturn(0);
        assertEquals(0, trainDao.update(mockTrain));
    }

    @Test
    void testWhenCallUpdateTrain_ThenReturnOne() {
        when(mockJdbcTemplate.update(UPDATE_TRAIN, mockTrain.getNumber(),
                mockTrain.getModel(), mockTrain.getDepartureStation(), mockTrain.getArrivalStation(), mockTrain.getId())).thenReturn(1);
        assertEquals(1, trainDao.update(mockTrain));

    }

}